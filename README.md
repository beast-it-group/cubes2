
======

This web application has been created by [BeastIt][Website].

Requirements
------------

* PHP 8.0.* or higher
* MariaDb 10 or higher
* and the [usual Symfony v5 requirements][Symfony Req].


Configuration
-------------
This project use Docker containers.
You need to have [Docker][Docker website] and [Docker-compose][Docker-compose documentation] installed (and eventually [Docker Desktop][Docker Desktop page] according to your OS).
After cloning this repo, setup project base by executing :
```bash
$ docker-compose up --build
```

Once the container will be active, vendor libraries will be installed & database will be created (with migrations if needed). \
As long as container will be running, project can be accessible on this URL :
```bash
http://localhost
```

The use of Symfony CLI tool is recommended, if you aren't already using it, you should [install it first][Symfony CLI] \
IF you choose to install the CLI, you can use the local web server provided (see documentation for HTTPS support & proxy use)

\
All configurations on .env file are default values. \
You should create a **.env.local** file with needed configuration for dev environment. \
⚠️ **In order to use docker image successfully, you will have to keep in .env file the *DATABASE_URL* environment variable**

\
A Makefile is at your disposal for convenients shortcuts, you can display all available by running :
```bash
$ make
```

Assets
-----
To intall then build the assets you can run the following commands

```bash
$ yarn install
$ yarn build
```

Tests & Quality Code
-----
For mail delivery during tests, we recommend to use [Mailtrap][Mailtrap website].  
Configuration needed is detailled on Mailtrap but basically you only need to use a specific MAILER_DSN env value.

If any tests are available, execute these commands to run tests :
```bash
$ ./bin/phpunit
```
```bash
$ vendor/bin/behat
```

2 differents quality code tools are installed in this base project, you can use either one or both of them :\
To run [PHP-cs-fixer][PHPCSFixer] use :
```bash
$ ./vendor/bin/php-cs-fixer fix
```

About
-----


[Website]: https://beastit.com
[Mailtrap website]: https://www.mailtrap.io
[Symfony Req]: https://symfony.com/doc/current/setup.html
[Symfony CLI]: https://symfony.com/download
[PHPCSFixer]: https://cs.symfony.com/
[PHPStan doc]: https://phpstan.org/user-guide/getting-started
[Docker website]: https://www.docker.com/
[Docker-compose documentation]: https://docs.docker.com/compose/
[Docker Desktop page]: https://www.docker.com/products/docker-desktop