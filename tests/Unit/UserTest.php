<?php

namespace App\Tests\Unit;

use App\Entity\User;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testIsTrue(): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; ++$i) {
            $firstName = $faker->firstName();
            $lastName = $faker->lastName();
            $email = $faker->email();
            $password = $faker->password();

            $user = (new User())
                ->setFirstName($firstName)
                ->setLastName($lastName)
                ->setActive(true)
                ->setPlainPassword($password)
                ->setEmail($email);

            $this->assertTrue($user->getFirstName() === $firstName);
            $this->assertTrue($user->getLastName() === $lastName);
            $this->assertTrue(true === $user->isActive());
            $this->assertTrue($user->getEmail() === $email);
            $this->assertTrue($user->getPlainPassword() === $password);
        }
    }

    public function testIsFalse(): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; ++$i) {
            $firstName = $faker->firstName();
            $lastName = $faker->lastName();
            $email = $faker->email();
            $password = $faker->password();

            $user = (new User())
                ->setFirstName($firstName)
                ->setLastName($lastName)
                ->setActive(true)
                ->setPlainPassword($password)
                ->setEmail($email);

            $this->assertFalse('False' === $user->getFirstName());
            $this->assertFalse('False' === $user->getLastName());
            $this->assertFalse(false === $user->isActive());
            $this->assertFalse('false@test.com' === $user->getEmail());
        }

    }
}
