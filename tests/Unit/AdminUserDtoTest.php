<?php

namespace App\Tests\Unit;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use PHPUnit\Framework\TestCase;

class AdminUserDtoTest extends TestCase
{
    public function testCreateFromDto(): void
    {
        $adminUserDto = null;
        $this->assertNull($adminUserDto);

        $adminUserDto = (new AdminUserDto())
            ->setEmail('test@test.com')
            ->setFirstName('FirstName')
            ->setLastName('LastName')
            ->setActive(true)
            ->setRole(AdminUser::ROLE_ADMIN)
        ;

        $this->assertClassHasAttribute('email', AdminUserDto::class);
        $this->assertClassHasAttribute('firstName', AdminUserDto::class);
        $this->assertClassHasAttribute('lastName', AdminUserDto::class);
        $this->assertClassHasAttribute('active', AdminUserDto::class);
        $this->assertClassHasAttribute('role', AdminUserDto::class);

        $this->assertTrue($adminUserDto->getEmail() === 'test@test.com');
        $this->assertTrue($adminUserDto->getFirstName() === 'FirstName');
        $this->assertTrue($adminUserDto->getLastName() === 'LastName');
        $this->assertTrue($adminUserDto->isActive());
        $this->assertTrue($adminUserDto->getRole() === AdminUser::ROLE_ADMIN);


        $adminUser = null;
        $this->assertNull($adminUser);

        $adminUser = (new AdminUser())
            ->setEmail($adminUserDto->getEmail())
            ->setFirstName($adminUserDto->getFirstName())
            ->setLastName($adminUserDto->getLastName())
            ->setRoles([$adminUserDto->getRole()])
            ->setActive($adminUserDto->isActive());

        $this->assertClassHasAttribute('email', AdminUser::class);
        $this->assertClassHasAttribute('firstName', AdminUser::class);
        $this->assertClassHasAttribute('lastName', AdminUser::class);
        $this->assertClassHasAttribute('active', AdminUser::class);
        $this->assertClassHasAttribute('roles', AdminUser::class);

        $this->assertTrue($adminUser->getEmail() === 'test@test.com');
        $this->assertTrue($adminUser->getFirstName() === 'FirstName');
        $this->assertTrue($adminUser->getLastName() === 'LastName');
        $this->assertTrue($adminUser->isActive());
        $this->assertContains(AdminUser::ROLE_ADMIN, $adminUser->getRoles());
    }
}
