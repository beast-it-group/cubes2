<?php

namespace App\Tests\Unit;

use App\Entity\Dummy\DummyGroup;
use App\Entity\Dummy\DummyUser;
use App\Entity\GroupInterface;
use App\Entity\Resource;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\UserInterface;

class ResourceTest extends TestCase
{
    protected ?Resource $resource = null;

    /**
     * @var Collection|User[]
     */
    protected Collection|array|null $users = null;

    public function createResource(): Resource
    {
        $dummyUser = new DummyUser();
        $dummyGroup = new DummyGroup();

        return (new Resource())
            ->setActive(true)
            ->setExploitationStatus(Resource::STATUS_EXPLOITED)
            ->setName('Vidéo sur Abraham Maslow')
            ->setParentGroup($dummyGroup)
            ->setType(Resource::TYPE_VIDEO)
            ->setAuthor($dummyUser)
            ->setVideoUrl('https://www.youtube.com/watch?v=wvaviPQyPQc')
        ;
    }

    public function testParentGroupIsAGroup()
    {
        $this->resource = $this->createResource();

        $this->assertTrue($this->resource->getParentGroup() instanceof GroupInterface, 'The parent group is indeed an instance of GroupInterface');
    }

    public function testAuthorIsAUser()
    {
        $this->resource = $this->createResource();

        $this->assertTrue($this->resource->getAuthor() instanceof UserInterface, 'The Author is indeed an instance of User');
    }

    public function testTypeIsAmongstTheTypeSelection()
    {
        $this->resource = $this->createResource();

        $this->assertContains($this->resource->getType(), [
            Resource::TYPE_VIDEO,
            Resource::TYPE_ACTIVITY,
            Resource::TYPE_ARTICLE,
            Resource::TYPE_CHALLENGE_CARD,
            Resource::TYPE_ONLINE_GAME,
            Resource::TYPE_PDF_COURSE,
            Resource::TYPE_WORKSHOP,
            Resource::TYPE_READING_NOTE,
        ]);
    }
}
