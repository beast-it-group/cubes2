<?php

namespace App\Tests\Controller;

use App\Entity\Group;
use App\Entity\Resource;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ResourceTest extends WebTestCase
{
    public function testPostingResourceWhileLoggedIn(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $groupRepository = static::getContainer()->get(GroupRepository::class);
        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('johndoe@cubes.com');
        $testGroup = $groupRepository->findOneBy(['type' => Group::TYPE_PUBLIC]);
        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('POST', '/api/resources', [
            'name' => 'video test',
            'type' => Resource::TYPE_VIDEO,
            'parentGroup' => $testGroup->getId(),
            'videoUrl' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
            'author' => $testUser->getId(),
        ]);
        $this->assertResponseIsSuccessful();
    }

    public function testGetResourceWhileLoggedIn()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('johndoe@cubes.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/api/resources/1');
        $this->assertResponseIsSuccessful();
    }
}
