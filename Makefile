# SETUP ------
EXEC_PHP = php
SYMFONY = $(EXEC_PHP) bin/console
SYMFONY_BIN = symfony
.DEFAULT_GOAL = help

## -- Base commands: Can be used to run the project quickly
build:
	docker-compose up --build -d

run:
	docker-compose up -d

migrate:
	docker exec -it beastit_php symfony console doctrine:migration:migrate --allow-no-migration -n

populate:
	docker exec -it beastit_php symfony console doctrine:fixtures:load -n

setup:
	make build
	make migrate
	make populate

start:
	make run
	make migrate

## —— Help list ———————————————————————————————————————————————————————————————
help:
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Composer (used with Symfony Binary)  ————————————————————————————————————
install: composer.json ## Install vendors according to the current composer.json file
	$(SYMFONY_BIN) composer install --optimize-autoloader

update: composer.json ## Update vendors according to the composer.json file
	$(SYMFONY_BIN) composer update

## —— Symfony 🎵 ———————————————————————————————————————————————————————————————
sf: ## List all Symfony commands
	$(SYMFONY_BIN)

cc: ## Clear the cache. DID YOU CLEAR YOUR CACHE????
	$(SYMFONY_BIN) console c:c

warmup: cc
	$(SYMFONY_BIN) console cache:warmup

fix-perms: ## Fix permissions of all var files
	chmod -R 777 var/*

assets: purge ## Install the assets with symlinks in the public folder
	$(SYMFONY_BIN) assets:install public/ --symlink --relative

purge: ## Purge cache and logs
	rm -rf var/cache/* var/logs/*

## —— Symfony binary 💻 ————————————————————————————————————————————————————————
bin-install: ## Download and install the binary in the project (file is ignored)
	curl -sS https://get.symfony.com/cli/installer | bash
	mv ~/.symfony/bin/symfony .

cert-install:  ## Install the local HTTPS certificates
	$(SYMFONY_BIN) server:ca:install

serve: ## Serve the application with HTTPS support
	$(SYMFONY_BIN) serve

serve_daemon: ## Serve the application on background
	$(SYMFONY_BIN) serve --daemon --port=8000

unserve: ## Stop the webserver
	$(SYMFONY_BIN) server:stop

## —— Yarn 🐱 / JavaScript —————————————————————————————————————————————————————
dev: ## Rebuild assets for the dev env
	yarn install
	yarn run encore dev

watch: ## Watch files and build assets when needed for the dev env
	yarn run encore dev --watch

build: ## Build assets for production
	yarn run encore production

lint: ## Lints Js files
	npx eslint assets/js --fix

## —— Messenger 📜 ————————————————————————————————————————————————————————
consume: ## Consume message in bus
	$(SYMFONY_BIN) console messenger:consume

## —— Mercure 📜 ——————————————————————————————————————————————————————————
mercure_server: ## Launch mercure server on port 3000
	bin/mercure/mercure --jwt-key='!ChangeMe!' --addr=':3000' --debug --allow-anonymous --cors-allowed-origins='https://localhost:8000' --publish-allowed-origins='http://localhost:3000'

mercure_caddy: ## Launch mercure caddy server on port 3000 with DEV config
	MERCURE_PUBLISHER_JWT_KEY='!ChangeMe!' MERCURE_SUBSCRIBER_JWT_KEY='!ChangeMe!' bin/mercure-caddy/mercure run -config bin/mercure-caddy/Caddyfile.dev