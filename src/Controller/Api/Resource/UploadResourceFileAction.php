<?php

namespace App\Controller\Api\Resource;

use App\Entity\Resource;
use App\Service\Group\GroupService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
class UploadResourceFileAction extends AbstractController
{
    public function __invoke(Request $request,
                             GroupService $groupService,
                             EntityManagerInterface $em): Resource
    {
        $uploadedFile = $request->files->get('resourceFile');
        $videoUrl = $request->request->get('videoUrl');
        if (!$uploadedFile) {
            if (!$videoUrl) {
                throw new BadRequestHttpException('"videoUrl or file" is required');
            }
        }
        $resource = (new Resource())
            ->setName($request->get('name'))
            ->setType($request->get('type'))
            ->setParentGroup($groupService->find(intval($request->get('parentGroup'))))
            ->setResourceFile($uploadedFile)
            ->setAuthor($this->getUser())
            ->setActive(true)
            ->setVideoUrl($request->get('videoUrl'))
        ;

        $em->persist($resource);
        $em->flush();

        return $resource;
    }
}