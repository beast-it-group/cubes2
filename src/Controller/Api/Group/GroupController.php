<?php

namespace App\Controller\Api\Group;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/group/', name: 'api_group_')]
class GroupController extends AbstractController
{
    #[Route('/group', name: 'group')]
    public function index(): Response
    {
        return $this->render('api/group/group/index.html.twig', [
            'controller_name' => 'GroupController',
        ]);
    }
}
