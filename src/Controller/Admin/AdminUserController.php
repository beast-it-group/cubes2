<?php

namespace App\Controller\Admin;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use App\Event\AdminUserActivationChangeEvent;
use App\Event\AdminUserConversionEvent;
use App\Event\AdminUserCreateEvent;
use App\Event\AdminUserDeleteEvent;
use App\Event\AdminUserDtoModifiedEvent;
use App\Form\AdminUserType;
use App\Repository\AdminUserRepository;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\UX\Turbo\Stream\TurboStreamResponse;

#[Route('utilisateurs', name: 'admin_user_')]
#[IsGranted('ROLE_ADMIN', message: 'Vous ne disposez pas des droits nécessaires pour effectuer cette action')]
class AdminUserController extends AbstractController implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    #[Route('/', name: 'index', methods: 'GET')]
    public function index(AdminUserRepository $adminUserRepository): Response
    {
        return $this->render('admin/admin_user/index.html.twig', ['users' => $adminUserRepository->findAll()]);
    }

    #[Route('/ajouter', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $form = $this->createForm(AdminUserType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO When UniqueEntity can be applied on DTO Object (planned for V6.1) update EventSubscriber since it will be redundant to call validation from its listener.
            $event = new AdminUserCreateEvent($form->getData());
            $this->eventDispatcher->dispatch($event, AdminUserCreateEvent::class);
            if ($event->getErrors()) {
                foreach ($event->getErrors() as $error) {
                    $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage(), $error->getMessageTemplate(), $error->getParameters(), null, $error->getCause()));
                }

                return $this->renderForm('admin/admin_user/new.html.twig', ['form' => $form]);
            }
            $this->addFlash('success', 'Nouvel utilisateur créé');

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->renderForm('admin/admin_user/new.html.twig', ['form' => $form]);
    }

    #[Route('/{id}/modifier', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, AdminUser $adminUser): Response
    {
        $event = new AdminUserConversionEvent($adminUser->getId(), new AdminUserDto());
        try {
            $this->eventDispatcher->dispatch($event, AdminUserConversionEvent::class);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['code' => $e->getCode()]);
            $this->addFlash('error', 'Une erreur est survenue. Utilisateur introuvable.');

            return $this->redirectToRoute('admin_user_index');
        }
        $form = $this->createForm(AdminUserType::class, $event->getAdminUserDto(), ['validation_groups' => 'ProfileUpdate']);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO When UniqueEntity can be applied on DTO Object (planned for V6.1) update EventSubscriber since it will be redundant to call validation from its listener.
            $event = new AdminUserDtoModifiedEvent($form->getData());
            $this->eventDispatcher->dispatch($event, AdminUserDtoModifiedEvent::class);
            if ($event->getErrors()) {
                foreach ($event->getErrors() as $error) {
                    $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage(), $error->getMessageTemplate(), $error->getParameters(), null, $error->getCause()));
                }

                return $this->renderForm('admin/admin_user/new.html.twig', ['form' => $form, 'user_full_name' => $adminUser->getFullName()]);
            }
            $this->addFlash('success', 'Utilisateur Modifié !');

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->renderForm('admin/admin_user/edit.html.twig', ['form' => $form, 'user_full_name' => $adminUser->getFullName()]);
    }

    #[Route('/{id}/activate-or-deactivate', name: 'activate_or_deactivate', methods: 'POST')]
    public function activateOrDeactivate(Request $request, AdminUser $adminUser): Response
    {
        if ($this->isCsrfTokenValid('activate_'.$adminUser->getId(), $request->request->get('_token'))) {
            $this->eventDispatcher->dispatch(new AdminUserActivationChangeEvent($adminUser), AdminUserActivationChangeEvent::class);
            $this->addFlash('success', sprintf('Utilisateur %s %s', $adminUser->getUserIdentifier(), ($adminUser->isActive() ? 'activé' : 'désactivé')));
        }

        if (TurboStreamResponse::STREAM_FORMAT === $request->getPreferredFormat()) {
            return $this->render('admin/admin_user/item.stream.html.twig', ['user' => $adminUser], new TurboStreamResponse());
        }

        return $this->redirectToRoute('admin_user_index');
    }

    #[Route('/{id}/supprimer', name: 'delete', methods: 'POST')]
    public function delete(Request $request, AdminUser $adminUser): Response
    {
        if ($this->isCsrfTokenValid('delete_'.$adminUser->getId(), $request->request->get('_token'))) {
            $this->eventDispatcher->dispatch(new AdminUserDeleteEvent($adminUser), AdminUserDeleteEvent::class);
            $this->addFlash('success', sprintf('Utilisateur %s supprimé', $adminUser->getUserIdentifier()));
        }

        return $this->redirectToRoute('admin_user_index');
    }
}
