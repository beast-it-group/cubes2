<?php

namespace App\Controller\Admin;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use App\Event\AdminUserConversionEvent;
use App\Event\AdminUserDtoModifiedEvent;
use App\Form\AdminUserType;
use App\Form\ProfilePasswordType;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route('/mon-profil', name: 'admin_profile_')]
class ProfileController extends AbstractController implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    #[Route('/{id<\d+>}/informations', name: 'index', methods: ['GET', 'POST'])]
    public function index(Request $request, AdminUser $adminUser): Response
    {
        if ($adminUser !== $this->getUser()) {
            throw new AccessDeniedHttpException('Vous n’avez pas les droits nécessaires pour effectuer cette action.');
        }

        $event = new AdminUserConversionEvent($adminUser->getId(), new AdminUserDto());
        try {
            $this->eventDispatcher->dispatch($event, AdminUserConversionEvent::class);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['code' => $e->getCode()]);
            $this->addFlash('error', 'Une erreur est survenue. Utilisateur introuvable.');

            return $this->redirectToRoute('admin_user_index');
        }

        $form = $this->createForm(ProfilePasswordType::class, $event->getAdminUserDto(), ['validation_groups' => 'UpdatePassword']);
        $infoForm = $this->createForm(AdminUserType::class, $event->getAdminUserDto(), ['validation_groups' => 'ProfileUpdate']);
        $infoForm->handleRequest($request);
        $form->handleRequest($request);
        if (($form->isSubmitted() && $form->isValid()) || ($infoForm->isSubmitted() && $infoForm->isValid())) {
            $handleInfoForm = $infoForm->isSubmitted();
            $event = new AdminUserDtoModifiedEvent($handleInfoForm ? $infoForm->getData() : $form->getData());
            $this->eventDispatcher->dispatch($event, AdminUserDtoModifiedEvent::class);
            if ($event->getErrors()) {
                foreach ($event->getErrors() as $error) {
                    if ($handleInfoForm) {
                        $infoForm->get($error->getPropertyPath())->addError(new FormError($error->getMessage(), $error->getMessageTemplate(), $error->getParameters(), null, $error->getCause()));
                    } else {
                        $form->get($error->getPropertyPath())->addError(new FormError($error->getMessage(), $error->getMessageTemplate(), $error->getParameters(), null, $error->getCause()));
                    }
                }

                return $this->renderForm('admin/profile/index.html.twig', ['form' => $form, 'info_form' => $infoForm]);
            }
            $this->addFlash('success', $handleInfoForm ? 'Vos informations ont bien été modifiées' : 'Votre mot de passe a bien été modifié');

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->renderForm('admin/profile/index.html.twig', ['form' => $form, 'info_form' => $infoForm]);
    }
}
