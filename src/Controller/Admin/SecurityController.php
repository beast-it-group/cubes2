<?php

namespace App\Controller\Admin;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use App\Event\AdminUserActivationEvent;
use App\Event\AdminUserConversionEvent;
use App\Form\ResettingType;
use App\Service\Auth\LoginAttemptLimitService;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route(name: 'admin_')]
class SecurityController extends AbstractController implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    private LoginAttemptLimitService $loginAttemptLimitService;

    public function __construct(LoginAttemptLimitService $loginAttemptLimitService)
    {
        $this->loginAttemptLimitService = $loginAttemptLimitService;
    }

    #[Route('/login', name: 'login', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
        if ($this->getUser()) {
            $this->addFlash('success', 'Vous êtes déjà connecté !');

            return $this->redirectToRoute('admin_dashboard');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();


        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'is_blocked' => $this->loginAttemptLimitService->hasExceedMaxLoginAttempts($lastUsername, $request->getClientIp()),
        ]);
    }

    #[Route('/logout', name: 'logout', methods: 'GET')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route('/activer-compte/{confirmationToken}', name: 'activate_account', methods: ['GET', 'POST'])]
    public function activateAccount(Request $request, AdminUser $adminUser, EventDispatcherInterface $eventDispatcher)
    {
        if ($adminUser->isActive() || null !== $adminUser->getLastLogin()) {
            $this->addFlash('info', 'Votre compte est déjà actif');

            return $this->redirectToRoute('admin_login');
        }

        $event = new AdminUserConversionEvent($adminUser->getId(), new AdminUserDto());
        try {
            $eventDispatcher->dispatch($event, AdminUserConversionEvent::class);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['code' => $e->getCode()]);
            $this->addFlash('error', 'Une erreur est survenue. Utilisateur introuvable.');

            return $this->redirectToRoute('admin_login');
        }
        $form = $this->createForm(ResettingType::class, $event->getAdminUserDto(), ['validation_groups' => 'ActivateAccount']);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $eventDispatcher->dispatch(new AdminUserActivationEvent($form->getData()));
            $this->addFlash('success', 'Votre compte a bien été activé ! Vous pouvez vous connecter');

            return $this->redirectToRoute('admin_login');
        }

        return $this->renderForm('admin/admin_user/activate_account.html.twig', ['form' => $form]);
    }
}
