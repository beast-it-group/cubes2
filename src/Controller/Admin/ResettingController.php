<?php

namespace App\Controller\Admin;

use App\Event\ResetPasswordEvent;
use App\Event\ResetPasswordRequestEvent;
use App\Form\ResettingRequestType;
use App\Form\ResettingType;
use App\Repository\AdminUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route('/mot-de-passe-oublie', name: 'admin_resetting_')]
class ResettingController extends AbstractController
{

    private EventDispatcherInterface $eventDispatcher;
    private AdminUserRepository $adminUserRepository;
    private EntityManagerInterface $em;

    public function __construct(EventDispatcherInterface $eventDispatcher, EntityManagerInterface $em, AdminUserRepository $adminUserRepository)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->adminUserRepository = $adminUserRepository;
        $this->em = $em;
    }

    #[Route('/', name: 'request', methods: ['GET', 'POST'])]
    public function index(Request $request): Response
    {
        $form = $this->createForm(ResettingRequestType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $adminUser = $this->adminUserRepository->findOneBy(['email' => $form->get('email')->getData(), 'active' => true]);
            if (!$adminUser) {
                $form->get('email')->addError(new FormError('Aucun compte associé a cette adresse email.'));

                return $this->renderForm('admin/resetting/index.html.twig', ['form' => $form]);
            }
            $this->eventDispatcher->dispatch(new ResetPasswordRequestEvent($adminUser));
            $this->em->persist($adminUser);
            $this->em->flush();

            return $this->redirectToRoute('admin_resetting_check_email', ['email' => $adminUser->getUserIdentifier()]);
        } elseif ($form->isSubmitted()) {
            return $this->renderForm('admin/resetting/index.html.twig', ['form' => $form]);
        }

        return $this->render('admin/resetting/index.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/envoi-email', name: 'check_email', methods: 'GET')]
    public function requestEmailSent(Request $request): Response
    {
        $email = $request->query->get('email');
        if (!$email) {
            return $this->redirectToRoute('admin_resetting_request');
        }

        return $this->render('admin/resetting/check_email.html.twig', ['email' => $email]);
    }

    #[Route('/renvoi-email/{email}', name: 'retry_send_email', methods: 'GET')]
    public function retryIfEmailNotReceived(Request $request, string $email): RedirectResponse
    {
        if (!$email) {
            return $this->redirectToRoute('admin_resetting_request');
        }

        $adminUser = $this->adminUserRepository->findOneBy(['email' => $email, 'active' => true]);
        if (!$adminUser) {
            $this->addFlash('error', 'Aucun compte associé a cette adresse email.');

            return $this->redirectToRoute('admin_resetting_request');
        }
        $this->eventDispatcher->dispatch(new ResetPasswordRequestEvent($adminUser));
        $this->em->persist($adminUser);
        $this->em->flush();
        $this->addFlash('success', 'Un nouvel email vient de vous être envoyé.');

        return $this->redirectToRoute('admin_resetting_check_email', ['email' => $adminUser->getUserIdentifier()]);
    }

    #[Route('/{token}', name: 'reset', methods: ['GET', 'POST'])]
    public function resetPassword(Request $request, string $token): Response
    {
        $adminUser = $this->adminUserRepository->findOneBy(['confirmationToken' => $token, 'active' => true]);
        if (!$adminUser) {
            $this->addFlash('error', 'Utilisateur introuvable. Veuillez renouveler votre demande.');

            return $this->redirectToRoute('admin_login');
        }

        $form = $this->createForm(ResettingType::class, $adminUser);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->eventDispatcher->dispatch(new ResetPasswordEvent($adminUser, true));
            $this->em->persist($adminUser);
            $this->em->flush();
            $this->addFlash('success', 'Votre nouveau mot de passe a bien été enregistré !');

            return $this->redirectToRoute('admin_login');

        } elseif ($form->isSubmitted()) {
            return $this->renderForm('admin/resetting/reset.html.twig', ['form' => $form]);
        }

        return $this->render('admin/resetting/reset.html.twig', ['form' => $form->createView()]);
    }
}
