<?php

namespace App\Event;

use App\Dto\AdminUserDto;
use Symfony\Contracts\EventDispatcher\Event;

class AdminUserActivationEvent extends Event
{
    private AdminUserDto $adminUserDto;

    public function __construct(AdminUserDto $adminUserDto)
    {
        $this->adminUserDto = $adminUserDto;
    }

    /**
     * @return AdminUserDto
     */
    public function getAdminUserDto(): AdminUserDto
    {
        return $this->adminUserDto;
    }
}
