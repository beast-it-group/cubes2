<?php

namespace App\Event;

use App\Entity\AuthenticatedEmailUserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class ResetPasswordRequestEvent extends Event
{
    private AuthenticatedEmailUserInterface $user;

    public function __construct(AuthenticatedEmailUserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser(): AuthenticatedEmailUserInterface
    {
        return $this->user;
    }
}
