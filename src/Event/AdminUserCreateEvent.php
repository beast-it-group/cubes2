<?php

namespace App\Event;

use App\Dto\AdminUserDto;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Contracts\EventDispatcher\Event;

class AdminUserCreateEvent extends Event
{
    private AdminUserDto $adminUserDto;
    private ?ConstraintViolationListInterface $errors;
    private string $validationGroups;

    public function __construct(AdminUserDto $adminUserDto, string $validationGroups = 'Default', $errors = null)
    {
        $this->adminUserDto = $adminUserDto;
        $this->validationGroups = $validationGroups;
        $this->errors = $errors;
    }

    /**
     * @return AdminUserDto
     */
    public function getAdminUserDto(): AdminUserDto
    {
        return $this->adminUserDto;
    }

    /**
     * @return string|null
     */
    public function getValidationGroups(): ?string
    {
        return $this->validationGroups;
    }

    public function getErrors(): ?ConstraintViolationListInterface
    {
        return $this->errors;
    }

    public function setErrors(?ConstraintViolationListInterface $errors): void
    {
        $this->errors = $errors;
    }
}
