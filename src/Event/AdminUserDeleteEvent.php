<?php

namespace App\Event;

use App\Entity\AdminUser;
use Symfony\Contracts\EventDispatcher\Event;

class AdminUserDeleteEvent extends Event
{
    private AdminUser $adminUser;

    public function __construct(AdminUser $adminUser)
    {
        $this->adminUser = $adminUser;
    }

    /**
     * @return AdminUser
     */
    public function getAdminUser(): AdminUser
    {
        return $this->adminUser;
    }
}
