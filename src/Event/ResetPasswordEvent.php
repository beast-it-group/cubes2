<?php

namespace App\Event;

use App\Entity\AuthenticatedEmailUserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class ResetPasswordEvent extends Event
{

    private AuthenticatedEmailUserInterface $user;
    private bool $sendEmailConfirmation;

    public function __construct(AuthenticatedEmailUserInterface $user, bool $sendEmailConfirmation = false)
    {
        $this->user = $user;
        $this->sendEmailConfirmation = $sendEmailConfirmation;
    }

    public function getUser(): AuthenticatedEmailUserInterface
    {
        return $this->user;
    }

    public function getSendEmailConfirmation(): bool
    {
        return $this->sendEmailConfirmation;
    }
}
