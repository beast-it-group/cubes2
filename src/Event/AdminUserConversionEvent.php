<?php

namespace App\Event;

use App\Dto\AdminUserDto;
use Symfony\Contracts\EventDispatcher\Event;

class AdminUserConversionEvent extends Event
{

    private int $adminUserId;
    private AdminUserDto $adminUserDto;

    public function __construct(int $adminUserId, AdminUserDto $adminUserDto)
    {
        $this->adminUserDto = $adminUserDto;
        $this->adminUserId = $adminUserId;
    }

    public function getAdminUserId(): int
    {
        return $this->adminUserId;
    }

    public function getAdminUserDto(): AdminUserDto
    {
        return $this->adminUserDto;
    }

    public function setAdminUserDto(?AdminUserDto $adminUserDto): void
    {
        $this->adminUserDto = $adminUserDto;
    }
}
