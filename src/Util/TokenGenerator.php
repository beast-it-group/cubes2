<?php

namespace App\Util;

class TokenGenerator
{
    public static function generateToken($length = 32): string
    {
        try {
            return rtrim(strtr(base64_encode(random_bytes($length)), '+/', '-_'), '=');
        } catch (\Exception $e) {
            return 'Error during token generation : '.$e->getMessage();
        }
    }
}
