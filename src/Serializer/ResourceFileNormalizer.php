<?php

namespace App\Serializer;

use App\Entity\Resource;
use App\Entity\ResourceInterface;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Vich\UploaderBundle\Storage\StorageInterface;

final class ResourceFileNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'RESOURCE_NORMALIZER_ALREADY_CALLED';

    public function __construct(private StorageInterface $storage, private UrlHelper $urlHelper, private string $resourceFilesUploadPath)
    {
    }

    public function normalize($object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $context[self::ALREADY_CALLED] = true;

        $object->setFile($this->getUrl($this->storage->resolveUri($object, 'resourceFile')));

        return $this->normalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Resource;
//        return $data instanceof ResourceInterface;
    }

    public function getUrl(?string $fileName, bool $absolute = true): ?string
    {
        if (empty($fileName)) return null;

        if ($absolute) {
            return $this->urlHelper->getAbsoluteUrl($this->resourceFilesUploadPath.$fileName);
        }

        return $this->urlHelper->getRelativePath($this->resourceFilesUploadPath.$fileName);
    }
}