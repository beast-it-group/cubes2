<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PasswordToggableType extends AbstractType
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (is_array($options['data-controller'])) {
            $view->vars['attr']['data-controller'] = implode(' ', $options['data-controller']);
        } else {
            $view->vars['attr']['data-controller'] = $options['data-controller'];
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('data-controller', 'password');
        $resolver->setAllowedTypes('data-controller', ['string', 'array']);
    }

    public function getParent(): string
    {
        return PasswordType::class;
    }
}
