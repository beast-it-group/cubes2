<?php

namespace App\Form;

use App\Entity\AdminUser;
use App\Form\Type\PasswordToggableType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;

class ResettingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'first_options' => [
                    'attr' => ['placeholder' => 'Nouveau mot de passe'],
                    'data-controller' => ['password', 'password-indicator'],
                    'label' => 'Nouveau mot de passe',
                ],
                'invalid_message' => 'Les deux mots de passe ne sont pas identiques',
                'second_options' => [
                    'attr' => ['placeholder' => 'Confirmer votre nouveau mot de passe'],
                    'data-controller' => ['password', 'password-indicator'],
                    'label' => 'Confirmer le mot de passe',
                ],
                'type' => PasswordToggableType::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_token_id' => 'resetting',
            'translation_domain' => false,
            'validation_groups' => ['ChangePassword'],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'resetting';
    }
}
