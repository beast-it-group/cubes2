<?php

namespace App\Form;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminUserType extends AbstractType
{
    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => ['placeholder' => 'Email'],
                'label' => 'Email',
            ])
            ->add('firstName', TextType::class, [
                'attr' => ['placeholder' => 'Prénom'],
                'label' => 'Prénom',
            ])
            ->add('lastName', TextType::class, [
                'attr' => ['placeholder' => 'Nom'],
                'label' => 'Nom',
            ])
            ->add('role', ChoiceType::class, [
                'choices' => !$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN') ? AdminUser::ROLE_CHOICES : array_merge(AdminUser::ROLE_CHOICES, ['Administrateur Technique' => AdminUser::ROLE_SUPER_ADMIN]),
                'label' => 'Rôle',
                'placeholder' => 'Choisir un rôle',
            ])
        ;
        if ($builder->getData() && $builder->getData()->getId()) {
            $builder->add('avatarFile', FileType::class, [
                'label' => 'Logo / Photo',
                'required' => false,
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AdminUserDto::class,
            'translation_domain' => false,
        ]);
    }
}
