<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ResettingRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'email@exemple.fr',
                ],
                'constraints' => [
                    new NotBlank(['message' => 'Veuillez indiquer une adresse e-mail valide']),
                    new Email(['message' => 'Cet email n\'est pas valide']),
                ],
                'label' => 'Email',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_token_id' => 'resetting_request',
            'translation_domain' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'resetting_request';
    }
}
