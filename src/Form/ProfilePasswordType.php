<?php

namespace App\Form;

use App\Form\Type\PasswordToggableType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfilePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('oldPassword', PasswordToggableType::class, [
                'attr' => [
                    'placeholder' => 'Ancien mot de passe',
                ],
                'label' => 'Ancien mot de passe',
            ])
            ->add('plainPassword', RepeatedType::class, [
                'first_options' => [
                    'attr' => ['placeholder' => 'Nouveau mot de passe'],
                    'data-controller' => ['password', 'password-indicator'],
                    'label' => 'Nouveau mot de passe',
                ],
                'invalid_message' => 'Les deux mots de passe ne sont pas identiques',
                'second_options' => [
                    'attr' => ['placeholder' => 'Confirmer votre nouveau mot de passe'],
                    'data-controller' => ['password', 'password-indicator'],
                    'label' => 'Confirmer le mot de passe',
                ],
                'type' => PasswordToggableType::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => false,
        ]);
    }
}
