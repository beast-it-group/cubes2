<?php

namespace App\Dto;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable()
 */
class AdminUserDto implements DtoInterface
{
    private ?int $id;

    #[Assert\NotBlank(message: "Les champs marqués d'un astérisque (*) sont obligatoires.", groups: ['Default', 'CommandCreate', 'ProfileUpdate'])]
    #[Assert\Email(message: "L'email {{ value }} est invalide", groups: ['Default', 'CommandCreate', 'ProfileUpdate'])]
    private ?string $email;

    #[Assert\NotBlank(message: "Les champs marqués d'un astérisque (*) sont obligatoires.", groups: ['Default', 'CommandCreate', 'ProfileUpdate'])]
    private ?string $lastName;

    #[Assert\NotBlank(message: "Les champs marqués d'un astérisque (*) sont obligatoires.", groups: ['Default', 'CommandCreate', 'ProfileUpdate'])]
    private ?string $firstName;

    #[Assert\NotBlank(message: "Les champs marqués d'un astérisque (*) sont obligatoires.", groups: ['CommandCreate', 'ActivateAccount', 'UpdatePassword'])]
    #[Assert\Length(
        min: 8,
        minMessage: 'Le mot de passe doit contenir au minimum 8 caractères.',
        groups: ['CommandCreate', 'ActivateAccount', 'UpdatePassword']
    )]
    #[Assert\Regex(
        pattern: '/([A-Z]+.*[0-9]+)|([0-9]+.*[A-Z]+)|([0-9]+.*[#@$%^&*()+=\-\[\]\';,:.\/{}|":<>!?~\\\\]+)/',
        message: 'Le mot de passe doit contenir au minimum 8 caractères, une majuscule, un chiffre et un caractère spécial.',
        groups: ['CommandCreate', 'ActivateAccount', 'UpdatePassword']
    )]
    #[Assert\Expression(
        'this.getPlainPassword() and this.getOldPassword()',
        message: 'Veuillez choisir un nouveau mot de passe valide pour en changer.',
        groups: ['UpdatePassword'],
    )]
    private ?string $plainPassword = null;

    #[Assert\Expression(
        'this.getPlainPassword() and this.getOldPassword()',
        message: 'Veuillez choisir un nouveau mot de passe valide pour en changer.',
        groups: ['UpdatePassword'],
    )]
    #[UserPassword(message: 'Ancien mot de passe incorrect.', groups: ['UpdatePassword'])]
    private ?string $oldPassword = null;

    #[Assert\NotBlank(message: "Les champs marqués d'un astérisque (*) sont obligatoires.", groups: ['Default', 'ProfileUpdate'])]
    private string $role;

    private bool $active = false;

    private ?string $avatar = null;

    /**
     * @Vich\UploadableField(mapping="admin_avatar", fileNameProperty="avatar")
     */
    #[Assert\File(
        maxSize: '4M',
        mimeTypes: ['image/jpeg', 'image/png'],
        maxSizeMessage: 'Le fichier est trop volumineux ({{size}} {{suffix}}). La taille maximum autorisée est {{limit}} {{suffix}}',
        mimeTypesMessage: 'Votre logo ou photo doit être de type PNG, JPG ou JPEG',
        groups: ['ProfileUpdate']
    )]
    private ?File $avatarFile = null;

    public function __construct(?int $id = null)
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getAvatarFile(): ?File
    {
        return $this->avatarFile;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function setOldPassword(?string $oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function setAvatarFile(?File $avatarFile = null): void
    {
        $this->avatarFile = $avatarFile;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
