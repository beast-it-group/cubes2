<?php

namespace App\Entity;

/**
 * Represents the interface that all User classes from the application must implement.
 *
 * This interface is used for reset password management.
 */
interface AuthenticatedEmailUserInterface
{
    /**
     * Returns the user's email (which is also the user identifier).
     */
    public function getEmail(): string;

    /**
     * Returns the user's full name (first name & last name grouped).
     */
    public function getFullName(): string;

    /**
     * Returns the token generated after :
     * - a reset password request
     * - a user creation.
     */
    public function getConfirmationToken(): ?string;

    /**
     * Set generated token.
     */
    public function setConfirmationToken(?string $confirmationToken);

    /**
     * Set password (must be hashed before).
     */
    public function setPassword(string $password);

    /**
     * Returns the plain password chosen by a user on password reset request.
     */
    public function getPlainPassword(): string;

    /**
     * Set the plain password value.
     */
    public function setPlainPassword(string $plainPassword);

    /**
     * Returns last login date (if existing, null otherwise).
     */
    public function getLastLogin(): ?\DateTimeImmutable;

    /**
     * Set last login date (mostly use onAuthenticationSuccess).
     */
    public function setLastLogin(?\DateTimeImmutable $lastLogin);

    /**
     * Returns last date where password reset request was made.
     */
    public function getPasswordRequestedAt(): ?\DateTimeImmutable;

    /**
     * Set the last date where a reset password request was made.
     */
    public function setPasswordRequestedAt(?\DateTimeImmutable $passwordRequestedAt);
}
