<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
#[ORM\HasLifecycleCallbacks]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => Group::GROUP_READ],
//            'security' => "user in object.getUsers().getValues() or object.getType() !== 'private'",
        ],
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => ['groups' => Group::GROUP_READ],
            'security' => "user in object.getUsers().getValues() or object.getType() !== 'private'",
        ],
        ],
    subresourceOperations: [
        'resources_get_subresource' => [
            'method' => 'GET',
            'path' => '/groups/{id}/newsfeed',
        ],
    ],
    order: ['type' => 'ASC'],
    paginationEnabled: false,
)]
class Group implements GroupInterface
{
    public const NEWSFEED_READ = 'newsfeed:read';

    public const GROUP_READ = 'group:read';
    public const GROUP_CREATE = 'group:create';
    public const GROUP_UPDATE = 'group:update';

    public const ROLE_GROUP_MEMBER = 'ROLE_USER';
    public const ROLE_GROUP_MODERATOR = 'ROLE_GROUP_MODERATOR';
    public const ROLE_GROUP_ADMIN = 'ROLE_GROUP_ADMIN';

    public const ROLE_CHOICES = [
        'Membre' => self::ROLE_GROUP_MEMBER,
        'Modérateur' => self::ROLE_GROUP_MODERATOR,
        'Administrateur' => self::ROLE_GROUP_ADMIN,
    ];

    public const TYPE_PUBLIC = 'public';
    public const TYPE_LIMITED = 'limited';
    public const TYPE_PRIVATE = 'private';

    public const TYPE_CHOICES = [
        'Public' => self::TYPE_PUBLIC,
        'Limité' => self::TYPE_LIMITED,
        'Privé' => self::TYPE_PRIVATE,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups([Group::GROUP_READ, Group::NEWSFEED_READ])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::GROUP_READ, Group::NEWSFEED_READ])]
    private string $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::GROUP_READ, Group::NEWSFEED_READ])]
    private string $type;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups([Group::GROUP_READ])]
    private ?\DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups([Group::GROUP_READ])]
    private ?\DateTimeImmutable $updatedAt;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'groups')]
    #[ApiSubresource]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'parentGroup', targetEntity: Resource::class, orphanRemoval: true)]
    #[ApiSubresource]
    private Collection $resources;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->resources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function setCreatedAt(): void
    {
        $this->createdAt = (new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris'));
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function setUpdatedAt(): void
    {
        $this->updatedAt = (new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris'));
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getResources(): Collection
    {
        return $this->resources;
    }

    public function addResource(ResourceInterface $resource): self
    {
        if (!$this->resources->contains($resource)) {
            $this->resources[] = $resource;
            $resource->setParentGroup($this);
        }

        return $this;
    }

    public function removeResource(ResourceInterface $resource): self
    {
        if ($this->resources->removeElement($resource)) {
            // set the owning side to null (unless already changed)
            if ($resource->getParentGroup() === $this) {
                $resource->setParentGroup(null);
            }
        }

        return $this;
    }
}
