<?php

namespace App\Entity\Dummy;

use App\Entity\ResourceInterface;
use Exception;

class DummyResource implements ResourceInterface
{
    /**
     * @throws Exception
     */
    public function getName()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    public function getType()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    public function getAuthor()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    public function getParentGroup()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    private function throwException()
    {
        throw new Exception('NullPointerException');
    }
}
