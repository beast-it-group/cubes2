<?php

namespace App\Entity\Dummy;

use App\Entity\GroupInterface;
use Exception;

class DummyGroup implements GroupInterface
{
    /**
     * @throws Exception
     */
    public function getName()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    public function getType()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    private function throwException()
    {
        throw new Exception('NullPointerException');
    }
}
