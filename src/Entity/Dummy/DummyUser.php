<?php

namespace App\Entity\Dummy;

use Exception;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method string getUserIdentifier()
 */
class DummyUser implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @throws Exception
     */
    public function getRoles(): ?array
    {
        $this->throwException();
        return null;
    }

    /**
     * @throws Exception
     */
    public function getPassword(): ?string
    {
        $this->throwException();
        return null;
    }

    /**
     * @throws Exception
     */
    public function getSalt(): ?string
    {
        $this->throwException();
        return null;
    }

    /**
     * @throws Exception
     */
    public function eraseCredentials()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    public function getUsername(): ?string
    {
        $this->throwException();
        return null;
    }

    public function __call(string $name, array $arguments)
    {
        $this->throwException();
    }

    private function throwException()
    {
        throw new Exception('NullPointerException');
    }
}
