<?php

namespace App\Entity;

interface ResourceInterface
{
    public function getName();
    public function getType();
    public function getAuthor();
    public function getParentGroup();
}