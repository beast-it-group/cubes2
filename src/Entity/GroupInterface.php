<?php

namespace App\Entity;

interface GroupInterface
{
    public function getName();

    public function getType();
}