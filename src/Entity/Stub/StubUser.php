<?php

namespace App\Entity\Stub;

use Exception;
use Symfony\Component\Security\Core\User\UserInterface;

class StubUser implements UserInterface
{
    /**
     * @throws Exception
     */
    public function getRoles(): array
    {
        $this->throwException();

        return [];
    }

    /**
     * @throws Exception
     */
    public function getPassword(): ?string
    {
        $this->throwException();

        return null;
    }

    /**
     * @throws Exception
     */
    public function getSalt(): ?string
    {
        $this->throwException();

        return null;
    }

    /**
     * @throws Exception
     */
    public function eraseCredentials()
    {
        $this->throwException();
    }

    /**
     * @throws Exception
     */
    public function getUsername(): string
    {
        $this->throwException();

        return '';
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return '';
    }

    /**
     * @throws Exception
     */
    private function throwException()
    {
        throw new Exception('NullPointerException');
    }
}
