<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Api\Resource\UploadResourceFileAction;
use App\Repository\ResourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: ResourceRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ApiResource(
    collectionOperations: [
        'get' => ['normalization_context' => ['groups' => Resource::RESOURCE_READ]],
        'post' => [
            'controller' => UploadResourceFileAction::class,
            'normalization_context' => ['groups' => Resource::RESOURCE_READ],
            'denormalization_context' => ['groups' => Resource::RESOURCE_CREATE],
            'deserialize' => false,
            'security' => 'is_granted(\'ROLE_USER\')',
            'openapi_context' => [
                'summary' => 'Create a resource',
                'description' => '#',
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'name' => ['type' => 'string'],
                                    'type' => ['type' => 'string', 'enum' => Resource::TYPE_CHOICES],
                                    'parentGroup' => ['type' => 'string', 'enum' => [1, 2, 3]],
                                    'videoUrl' => ['type' => 'string'],
                                    'resourceFile' => ['type' => 'string', 'format' => 'binary'],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => ['groups' => Resource::RESOURCE_READ],
            'security' => 'user in object.getparentGroup().getUsers().getValues()',
        ],
    ],
    subresourceOperations: [
        'api_groups_resources_get_subresource' => [
            'normalization_context' => ['groups' => [Group::NEWSFEED_READ]],
        ],
    ],
    order: ['name' => 'ASC', 'type' => 'ASC'],
    paginationEnabled: false,
)]
#[ApiFilter(SearchFilter::class, properties: ['group' => 'exact'])]
class Resource implements ResourceInterface
{
    public const RESOURCE_READ = 'resource:read';
    public const RESOURCE_CREATE = 'resource:create';
    public const RESOURCE_UPDATE = 'resource:update';

    public const STATUS_TRANSITION_TO_EXPLOITED = 'to_exploited';
    public const STATUS_EXPLOITED = 'exploited';

    public const STATUS_TRANSITION_TO_NON_EXPLOITED = 'to_non_exploited';
    public const STATUS_NON_EXPLOITED = 'non_exploited';

    public const STATUS_TRANSITION_TO_SET_APART = 'to_set_apart';
    public const STATUS_SET_APART = 'set_apart';

    public const TRANSITIONS = [
        self::STATUS_TRANSITION_TO_EXPLOITED => self::STATUS_EXPLOITED,
        self::STATUS_TRANSITION_TO_NON_EXPLOITED => self::STATUS_NON_EXPLOITED,
        self::STATUS_TRANSITION_TO_SET_APART => self::STATUS_SET_APART,
    ];

    public const FORMATTED_STATUS = [
        self::STATUS_EXPLOITED => 'Exploitée',
        self::STATUS_NON_EXPLOITED => 'Non exploitée',
        self::STATUS_SET_APART => 'Mise de côté',
    ];

    public const TYPE_ARTICLE = 'article';
    public const TYPE_ACTIVITY = 'activity';
    public const TYPE_ONLINE_GAME = 'online_game';
    public const TYPE_WORKSHOP = 'workshop';
    public const TYPE_CHALLENGE_CARD = 'challenge_card';
    public const TYPE_VIDEO = 'video';
    public const TYPE_PDF_COURSE = 'pdf_course';
    public const TYPE_READING_NOTE = 'reading_note';

    public const TYPE_CHOICES = [
        'Article' => self::TYPE_ARTICLE,
        'Activité / Jeu à réaliser' => self::TYPE_ACTIVITY,
        'Jeu en ligne' => self::TYPE_ONLINE_GAME,
        'Exercice / Atelier' => self::TYPE_WORKSHOP,
        'Carte défi' => self::TYPE_CHALLENGE_CARD,
        'Vidéo' => self::TYPE_VIDEO,
        'Cours au format PDF' => self::TYPE_PDF_COURSE,
        'Fiche de lecture' => self::TYPE_READING_NOTE,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups([Resource::RESOURCE_READ, Group::NEWSFEED_READ])]
    protected ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Resource::RESOURCE_READ, Resource::RESOURCE_CREATE, Group::NEWSFEED_READ])]
    protected string $name;

    #[ORM\Column(type: 'boolean', nullable: true)]
    #[Groups([Resource::RESOURCE_READ, Group::NEWSFEED_READ])]
    protected ?bool $active;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups([Resource::RESOURCE_READ, Group::NEWSFEED_READ])]
    protected string $exploitationStatus = self::STATUS_NON_EXPLOITED;

    #[ORM\Column(type: 'string', length: 100)]
    #[Groups([Resource::RESOURCE_READ, Resource::RESOURCE_CREATE, Group::NEWSFEED_READ])]
    protected string $type;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups([Resource::RESOURCE_READ, Group::NEWSFEED_READ])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups([Resource::RESOURCE_READ, Group::NEWSFEED_READ])]
    protected \DateTimeImmutable $updatedAt;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'favoriteResources')]
    protected Collection $favoredBy;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'resources')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([Resource::RESOURCE_READ, Group::NEWSFEED_READ])]
    protected $author;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups([Resource::RESOURCE_READ, Resource::RESOURCE_CREATE, Group::NEWSFEED_READ])]
    protected ?string $videoUrl = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[ApiProperty(required: false, iri: 'https://schema.org/contentUrl')]
    #[Groups([Resource::RESOURCE_READ, Resource::RESOURCE_CREATE, Group::NEWSFEED_READ])]
    protected ?string $file = null; // path towards file

    #[ApiProperty(iri: 'https://schema.org/image')]
    /**
     * @Vich\UploadableField(mapping="resource_file", fileNameProperty="file")
     */
    protected ?File $resourceFile = null;

    #[ORM\ManyToOne(targetEntity: Group::class, inversedBy: 'resources')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([Resource::RESOURCE_READ, Resource::RESOURCE_CREATE, Group::NEWSFEED_READ])]
    protected ?GroupInterface $parentGroup;

    public function __construct()
    {
        $this->favoredBy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getExploitationStatus(): ?string
    {
        return $this->exploitationStatus;
    }

    public function setExploitationStatus(string $exploitationStatus): self
    {
        $this->exploitationStatus = $exploitationStatus;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function setCreatedAt(): void
    {
        $this->createdAt = (new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris'));
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function setUpdatedAt(): void
    {
        $this->updatedAt = (new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris'));
    }

    public function getFavoredBy(): Collection
    {
        return $this->favoredBy;
    }

    public function addFavoredBy(UserInterface $favoredBy): self
    {
        if (!$this->favoredBy->contains($favoredBy)) {
            $this->favoredBy[] = $favoredBy;
            $favoredBy->addFavoriteResource($this);
        }

        return $this;
    }

    public function removeFavoredBy(UserInterface $favoredBy): self
    {
        if ($this->favoredBy->removeElement($favoredBy)) {
            $favoredBy->removeFavoriteResource($this);
        }

        return $this;
    }

    public function getAuthor(): ?UserInterface
    {
        return $this->author;
    }

    public function setAuthor(?UserInterface $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(?string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        if (null !== $file) {
            $this->setUpdatedAt();
        }

        return $this;
    }

    public function getResourceFile(): ?File
    {
        return $this->resourceFile;
    }

    public function setResourceFile(?File $resourceFile): self
    {
        $this->resourceFile = $resourceFile;
        $this->setUpdatedAt();

        return $this;
    }

    public function getParentGroup(): ?GroupInterface
    {
        return $this->parentGroup;
    }

    public function setParentGroup(?GroupInterface $parentGroup): self
    {
        $this->parentGroup = $parentGroup;

        return $this;
    }
}
