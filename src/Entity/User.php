<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(fields: ['email'], message: "L'email {{ value }} est déjà utilisé", groups: ['Default'])]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => User::USER_READ],
            'security' => "is_granted('ROLE_ADMIN')",
        ],
        'post' => [
            'normalization_context' => ['groups' => User::USER_READ],
            'denormalization_context' => ['groups' => User::USER_CREATE],
        ],
    ],
    itemOperations: [
        'get' => [
            'normalization_context' => ['groups' => User::USER_READ],
            'security' => "(is_granted('ROLE_USER') and object == user) or is_granted('ROLE_ADMIN')",
        ],
        'put' => [
            'denormalization_context' => ['groups' => [User::USER_UPDATE]],
            'security' => "(is_granted('ROLE_USER') and object == user) or is_granted('ROLE_ADMIN')",
        ],
        'delete' => [
            'security' => "(is_granted('ROLE_USER') and object == user) or is_granted('ROLE_ADMIN')",
        ],
    ],
)]
/**
 * @Vich\Uploadable
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface, AuthenticatedEmailUserInterface
{
    public const USER_READ = 'user:read';
    public const USER_CREATE = 'user:create';
    public const USER_UPDATE = 'user:update';

    public const ROLE_DEFAULT = 'ROLE_USER';
    public const ROLE_MODERATOR = 'ROLE_MODERATOR';

    public const ROLE_CHOICES = [
        'Citoyen' => self::ROLE_DEFAULT,
        'Modérateur' => self::ROLE_MODERATOR,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups([User::USER_READ, Group::NEWSFEED_READ])]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 150, unique: true)]
    #[Groups([User::USER_CREATE, User::USER_UPDATE, User::USER_READ, Group::NEWSFEED_READ])]
    private string $email;

    #[Groups([User::USER_CREATE, User::USER_UPDATE])]
    private string $plainPassword;

    #[Groups([User::USER_CREATE, User::USER_UPDATE])]
    private string $passwordConfirm;

    #[Groups(User::USER_UPDATE)]
    private ?string $oldPassword = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $password = null;

    #[ORM\Column(type: 'string', length: 150)]
    #[Groups([User::USER_CREATE, User::USER_UPDATE, User::USER_READ, Group::NEWSFEED_READ])]
    private ?string $firstName = null;

    #[ORM\Column(type: 'string', length: 150)]
    #[Groups([User::USER_CREATE, User::USER_UPDATE, User::USER_READ, Group::NEWSFEED_READ])]
    private ?string $lastName = null;

    #[ORM\Column(type: 'json')]
    #[Groups(User::USER_READ)]
    private ?array $roles = [];

    #[ORM\Column(type: 'boolean')]
    #[Groups(User::USER_READ)]
    private ?bool $active = null;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(User::USER_READ)]
    private ?\DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(User::USER_READ)]
    private ?\DateTimeImmutable $updatedAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(User::USER_READ)]
    private ?\DateTimeImmutable $lastLogin;

    #[ORM\ManyToMany(targetEntity: Group::class, mappedBy: 'users')]
    #[Groups(User::USER_READ)]
    private ?Collection $groups;

    #[ORM\ManyToMany(targetEntity: Resource::class, inversedBy: 'favoredBy')]
    #[Groups(User::USER_READ)]
    private ?Collection $favoriteResources;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private ?string $confirmationToken;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $passwordRequestedAt;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Resource::class)]
    #[Groups(User::USER_READ)]
    private ?Collection $resources;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->favoriteResources = new ArrayCollection();
        $this->resources = new ArrayCollection();
    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return 'user';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getPasswordConfirm(): string
    {
        return $this->passwordConfirm;
    }

    public function setPasswordConfirm(string $passwordConfirm): self
    {
        $this->passwordConfirm = $passwordConfirm;

        return $this;
    }

    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): self
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function setCreatedAt(): void
    {
        $this->createdAt = (new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris'));
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function setUpdatedAt(): void
    {
        $this->updatedAt = (new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris'));
    }

    public function getLastLogin(): ?\DateTimeImmutable
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeImmutable $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(GroupInterface $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addUser($this);
        }

        return $this;
    }

    public function removeGroup(GroupInterface $group): self
    {
        if ($this->groups->removeElement($group)) {
            $group->removeUser($this);
        }

        return $this;
    }

    public function getFavoriteResources(): Collection
    {
        return $this->favoriteResources;
    }

    public function addFavoriteResource(ResourceInterface $favoriteResource): self
    {
        if (!$this->favoriteResources->contains($favoriteResource)) {
            $this->favoriteResources[] = $favoriteResource;
        }

        return $this;
    }

    public function removeFavoriteResource(ResourceInterface $favoriteResource): self
    {
        $this->favoriteResources->removeElement($favoriteResource);

        return $this;
    }

    public function getFullName(): string
    {
        return ucfirst($this->firstName).' '.ucfirst($this->lastName);
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getPasswordRequestedAt(): ?\DateTimeImmutable
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeImmutable $passwordRequestedAt): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt->setTimezone(new \DateTimeZone('Europe/Paris'));

        return $this;
    }

    public function __serialize(): array
    {
        return [
            $this->id,
            $this->email,
            $this->roles,
            $this->password,
            $this->lastName,
            $this->firstName,
            $this->active,
            $this->confirmationToken,
        ];
    }

    public function __unserialize($data)
    {
        list(
            $this->id,
            $this->email,
            $this->roles,
            $this->password,
            $this->lastName,
            $this->firstName,
            $this->active,
            $this->confirmationToken) = $data;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = self::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function getRoleFormatted(): string
    {
        return array_flip(self::ROLE_CHOICES)[$this->getRoles()[0]];
    }

    public function addRole($role): self
    {
        $role = strtoupper($role);

        if (self::ROLE_DEFAULT !== $role && !in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = '';
        $this->oldPassword = '';
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getResources(): Collection
    {
        return $this->resources;
    }

    public function addResource(ResourceInterface $resource): self
    {
        if (!$this->resources->contains($resource)) {
            $this->resources[] = $resource;
            $resource->setAuthor($this);
        }

        return $this;
    }

    public function removeResource(ResourceInterface $resource): self
    {
        if ($this->resources->removeElement($resource)) {
            // set the owning side to null (unless already changed)
            if ($resource->getAuthor() === $this) {
                $resource->setAuthor(null);
            }
        }

        return $this;
    }
}
