<?php

namespace App\EventSubscriber;

use App\Event\AdminUserActivationEvent;
use App\Event\AdminUserConversionEvent;
use App\Event\AdminUserCreateEvent;
use App\Event\AdminUserDtoModifiedEvent;
use App\Service\AdminUser\AdminUserFactory;
use App\Service\AdminUser\AdminUserMailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdminUserDtoSubscriber implements EventSubscriberInterface
{

    private AdminUserFactory $adminUserFactory;
    private EntityManagerInterface $em;
    private AdminUserMailerService $adminUserMailerService;

    public function __construct(AdminUserFactory $adminUserFactory, EntityManagerInterface $em, AdminUserMailerService $adminUserMailerService)
    {
        $this->adminUserFactory = $adminUserFactory;
        $this->em = $em;
        $this->adminUserMailerService = $adminUserMailerService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AdminUserCreateEvent::class => [
                ['validateDto', 10],
                ['createAdminUserFromDtoEvent', 0],
            ],
            AdminUserConversionEvent::class => 'convertIntoDtoEvent',
            AdminUserActivationEvent::class => 'activateUserEvent',
            AdminUserDtoModifiedEvent::class => [
                ['validateDto', 10],
                ['updateAdminUserFromDto', 0],
            ],
        ];
    }

    public function createAdminUserFromDtoEvent(AdminUserCreateEvent $event)
    {
        $adminUserDto = $event->getAdminUserDto();
        $adminUser = $this->adminUserFactory->createFromDto($adminUserDto);
        $this->em->persist($adminUser);
        $this->em->flush();
        $this->adminUserMailerService->sendAccountCreationEmail($adminUser);
    }

    public function convertIntoDtoEvent(AdminUserConversionEvent $event)
    {
        $event->setAdminUserDto($this->adminUserFactory->convertToDto('id', $event->getAdminUserId()));
    }

    public function activateUserEvent(AdminUserActivationEvent $event)
    {
        $adminUser = $this->adminUserFactory->updateFromDto($event->getAdminUserDto());
        $adminUser
            ->setConfirmationToken(null)
            ->setActive(true)
        ;
        $this->em->persist($adminUser);
        $this->em->flush();
        $this->adminUserMailerService->sendActivationConfirmationEmail($adminUser);
    }

    /**
     * This method is used for DTO validation (mainly for UniqueEntity manual verification)
     * TODO Remove this method when UniqueEntity constraint can be applied on DTO.
     */
    public function validateDto(AdminUserDtoModifiedEvent|AdminUserCreateEvent $event)
    {
        $errors = $this->adminUserFactory->validateDto($event->getAdminUserDto(), $event->getValidationGroups());
        if ($errors->count() > 0) {
            $event->setErrors($errors);
            $event->stopPropagation();
        }
    }

    public function updateAdminUserFromDto(AdminUserDtoModifiedEvent $event)
    {
        if (!$event->isPropagationStopped()) {
            $adminUser = $this->adminUserFactory->updateFromDto($event->getAdminUserDto());
            $this->em->persist($adminUser);
            $this->em->flush();
        }
    }
}
