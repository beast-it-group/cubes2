<?php

namespace App\EventSubscriber;

use App\Entity\AdminUser;
use App\Entity\LoginAttempt;
use App\Service\Auth\LoginAttemptCleanerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginAttemptSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $em;
    private LoginAttemptCleanerService $loginAttemptCleanerService;

    public function __construct(EntityManagerInterface $em, LoginAttemptCleanerService $loginAttemptCleanerService)
    {
        $this->em = $em;
        $this->loginAttemptCleanerService = $loginAttemptCleanerService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            LoginFailureEvent::class => 'onAuthenticationFailure',
            SecurityEvents::INTERACTIVE_LOGIN => 'onAuthenticationSuccess',
        ];
    }

    public function onAuthenticationFailure(LoginFailureEvent $event)
    {
        $newLoginAttempt = new LoginAttempt($event->getRequest()->getClientIp(), $event->getRequest()->request->get('email', ''));
        $this->em->persist($newLoginAttempt);
        $this->em->flush();
    }

    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        /** @var AdminUser $adminUser */
        $adminUser = $event->getAuthenticationToken()->getUser();
        $adminUser->setLastLogin((new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris')));
        $this->loginAttemptCleanerService->clear($adminUser->getUserIdentifier());
        $this->em->flush();
    }
}
