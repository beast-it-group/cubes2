<?php

namespace App\EventSubscriber;

use App\Event\AdminUserDeleteEvent;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class AssetsAndCacheSubscriber implements EventSubscriberInterface
{

    private CacheManager $cacheManager;
    private UploaderHelper $uploaderHelper;

    public function __construct(CacheManager $cacheManager, UploaderHelper $uploaderHelper)
    {
        $this->cacheManager = $cacheManager;
        $this->uploaderHelper = $uploaderHelper;
    }

    public static function getSubscribedEvents(): array
    {
        return [AdminUserDeleteEvent::class => ['deleteAdminUserAssetsAndCache', 100]];
    }

    public function deleteAdminUserAssetsAndCache(AdminUserDeleteEvent $event)
    {
        $adminUser = $event->getAdminUser();
        if ($event->getAdminUser()->getAvatar()) {
            $this->cacheManager->remove($this->uploaderHelper->asset($adminUser, 'avatarFile'));
        }
    }
}
