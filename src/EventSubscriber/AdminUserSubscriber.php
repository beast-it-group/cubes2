<?php

namespace App\EventSubscriber;

use App\Event\AdminUserActivationChangeEvent;
use App\Event\AdminUserDeleteEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdminUserSubscriber implements EventSubscriberInterface
{

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AdminUserActivationChangeEvent::class => 'onAdminUserActivationChangeEvent',
            AdminUserDeleteEvent::class => 'onAdminUserDeleteEvent',
        ];
    }

    public function onAdminUserActivationChangeEvent(AdminUserActivationChangeEvent $event): void
    {
        $adminUser = $event->getAdminUser();
        $adminUser->setActive(!$adminUser->isActive());
        $this->em->persist($adminUser);
        $this->em->flush();
    }

    public function onAdminUserDeleteEvent(AdminUserDeleteEvent $event)
    {
        $this->em->remove($event->getAdminUser());
        $this->em->flush();
    }
}
