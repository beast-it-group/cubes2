<?php

namespace App\EventSubscriber;

use App\Event\ResetPasswordEvent;
use App\Event\ResetPasswordRequestEvent;
use App\Service\Auth\AuthMailerService;
use App\Util\TokenGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ResetPasswordSubscriber implements EventSubscriberInterface
{
    private UserPasswordHasherInterface $userPasswordHasher;
    private AuthorizationCheckerInterface $authorizationChecker;
    private AuthMailerService $authMailerService;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher, AuthorizationCheckerInterface $authorizationChecker, AuthMailerService $authMailerService)
    {
        $this->userPasswordHasher = $userPasswordHasher;
        $this->authorizationChecker = $authorizationChecker;
        $this->authMailerService = $authMailerService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ResetPasswordRequestEvent::class => 'onResetPasswordRequestEvent',
            ResetPasswordEvent::class => 'onResetPasswordEvent',
        ];
    }

    public function onResetPasswordRequestEvent(ResetPasswordRequestEvent $event)
    {
        $user = $event->getUser();
        if (!$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN', $user)) {
            $user->setPasswordRequestedAt((new \DateTimeImmutable())->setTimezone(new \DateTimeZone('Europe/Paris')));
            $user->setConfirmationToken(TokenGenerator::generateToken(16));
            $this->authMailerService->sendResetPasswordRequestEmail($user);
        }
    }

    public function onResetPasswordEvent(ResetPasswordEvent $event)
    {
        $user = $event->getUser();
        if (!$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN', $user)) {
            $user->setConfirmationToken(null);
            $user->setPassword($this->userPasswordHasher->hashPassword($user, $user->getPlainPassword()));
            if ($event->getSendEmailConfirmation()) {
                $this->authMailerService->sendResetPasswordConfirmationEmail($user);
            }
        }
    }
}
