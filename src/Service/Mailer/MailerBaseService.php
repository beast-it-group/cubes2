<?php

namespace App\Service\Mailer;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerBaseService implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    protected function createTemplatedEmail(string $htmlTemplate, ?string $txtTemplate = null, array $contextData = []): TemplatedEmail
    {
        $email = (new TemplatedEmail())
            ->htmlTemplate($htmlTemplate)
            ->context($contextData)
        ;
        if ($txtTemplate) {
            $email->textTemplate($txtTemplate);
        }

        return $email;
    }

    protected function send(Email $email): void
    {
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error(sprintf('Error while trying to send an email -> %s (code %s)', $e->getMessage(), $e->getCode()));
        }
    }
}
