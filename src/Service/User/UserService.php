<?php

namespace App\Service\User;

use App\Entity\Group;
use App\Entity\User;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    public function __construct(private UserRepository              $userRepository,
                                private GroupRepository             $groupRepository,
                                private UserPasswordHasherInterface $passwordHasher,

    )
    {
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->userRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function create(User $user): User
    {
        // TODO Add a mailer for account confirmation with token.
        $user
            ->setActive(true)
            ->setRoles([User::ROLE_DEFAULT])
            ->addGroup($this->groupRepository->findOneBy(['type' => Group::TYPE_PUBLIC]));

        if ($user->getPlainPassword() && $user->getPlainPassword() == $user->getPasswordConfirm()) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));
            $user->eraseCredentials();
        }

        return $user;
    }

    public function update(User $user): User
    {
        $updatedUser = $this->userRepository->find($user->getId());

        $updatedUser
            ->setEmail($user->getEmail())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setRoles($user->getRoles())
            ->setActive($user->isActive())
            ;
        if (($user->getOldPassword() == $updatedUser->getPlainPassword())
            && $user->getPlainPassword()
            && $user->getPlainPassword() == $user->getPasswordConfirm()) {
            $updatedUser->setPassword($this->passwordHasher->hashPassword($updatedUser, $user->getPlainPassword()));
            $updatedUser->eraseCredentials();
        }


        return $updatedUser;
    }

}