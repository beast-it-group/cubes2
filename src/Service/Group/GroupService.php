<?php

namespace App\Service\Group;

use App\Entity\Group;
use App\Repository\GroupRepository;
use App\Repository\ResourceRepository;

class GroupService
{
    public function __construct(private GroupRepository $groupRepository, private ResourceRepository $resourceRepository)
    {
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return $this->groupRepository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->groupRepository->findOneBy($criteria, $orderBy);
    }

    public function getPublicGroupFeed(): array
    {
        $publicGroup = $this->groupRepository->findOneBy(['type' => Group::TYPE_PUBLIC]);

        return $this->resourceRepository->findBy(['parentGroup' => $publicGroup], ['createdAt' => 'DESC']);
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        return $this->groupRepository->find($id, $lockMode, $lockVersion);

    }
}