<?php

namespace App\Service\Dto;

use App\Dto\DtoInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

interface DtoFactoryInterface
{
    public function validateDto(DtoInterface $dtoObject, string $validationGroup = 'Default'): ConstraintViolationListInterface;

    public function createFromDto(DtoInterface $dtoObject);

    public function updateFromDto(DtoInterface $dtoObject);

    public function convertToDto(string $identifier, int|string $value): DtoInterface;
}
