<?php

namespace App\Service\Auth;

use App\Entity\AuthenticatedEmailUserInterface;
use App\Service\Mailer\MailerBaseService;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AuthMailerService extends MailerBaseService
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $urlGenerator)
    {
        parent::__construct($mailer);
        $this->urlGenerator = $urlGenerator;
    }

    public function sendResetPasswordRequestEmail(AuthenticatedEmailUserInterface $user): void
    {
        $resetUrl = $this->urlGenerator->generate('admin_resetting_reset', ['token' => $user->getConfirmationToken()], UrlGeneratorInterface::ABSOLUTE_URL);
        $email = $this->createTemplatedEmail(
            'public/emails/resetting/request.html.twig',
            'public/emails/resetting/request.txt.twig',
            ['reset_password_url' => $resetUrl, 'user_full_name' => $user->getFullName()])
            ->subject('Réinitialisation de votre mot de passe')
            ->to(new Address($user->getEmail(), $user->getFullName()))
        ;

        $this->send($email);
    }

    public function sendResetPasswordConfirmationEmail(AuthenticatedEmailUserInterface $user): void
    {
        $email = $this->createTemplatedEmail(
            'public/emails/resetting/reset.html.twig',
            'public/emails/resetting/reset.txt.twig',
            ['user_full_name' => $user->getFullName()]
        )
            ->subject('Nouveau mot de passe enregistré')
            ->to(new Address($user->getEmail(), $user->getFullName()))
        ;

        $this->send($email);
    }
}
