<?php

namespace App\Service\Auth;

use App\Repository\LoginAttemptRepository;

class LoginAttemptLimitService
{
    private const MAX_ATTEMPTS = 5;
    private LoginAttemptRepository $loginAttemptRepository;

    public function __construct(LoginAttemptRepository $loginAttemptRepository)
    {
        $this->loginAttemptRepository = $loginAttemptRepository;
    }

    public function hasExceedMaxLoginAttempts(?string $username, ?string $clientIp): bool
    {
        return self::MAX_ATTEMPTS <= $this->loginAttemptRepository->countByRecent($username) || self::MAX_ATTEMPTS <= $this->loginAttemptRepository->countByRecent(null, $clientIp);
    }
}