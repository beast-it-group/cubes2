<?php

namespace App\Service\Auth;

use App\Repository\LoginAttemptRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Used to clear all existing LoginAttempt(s) for given User identifier (email).
 * Main use is on authentication success.
 */
class LoginAttemptCleanerService
{

    private LoginAttemptRepository $loginAttemptRepository;
    private EntityManagerInterface $em;

    public function __construct(LoginAttemptRepository $loginAttemptRepository, EntityManagerInterface $em)
    {
        $this->loginAttemptRepository = $loginAttemptRepository;
        $this->em = $em;
    }

    public function clear(string $userIdentifier): void
    {
        $loginAttempts = $this->loginAttemptRepository->findBy(['username' => $userIdentifier]);
        foreach ($loginAttempts as $loginAttempt) {
            $this->em->remove($loginAttempt);
        }
    }
}
