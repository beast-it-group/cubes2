<?php

namespace App\Service\Validator;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class FormErrorService
{
    /**
     * Returns an array with every error messages of a given form.
     *
     * @param FormView $form
     * @param array $errorMsgs [optional]
     * @param bool $recursive [optional] If true, will get the error message of the $form children (default: true)
     * @return array
     */
    public function getFormErrorMessages(FormView $form, array $errorMsgs = [], bool $recursive = true): array
    {
        if (isset($form->vars['errors'])) {
            foreach ($form->vars['errors'] as $error) {
                if (!in_array($error->getMessage(), $errorMsgs)) {
                    $errorMsgs[] = $error->getMessage();
                }
            }
        }
        if ($recursive) {
            foreach ($form->children as $childForm) {
                $errorMsgs = $this->getFormErrorMessages($childForm, $errorMsgs);
            }
        }

        return $errorMsgs;
    }

    /**
     * Returns an array with two keys according to a given form:
     *   - messages: Contains every error messages of the given form;
     *   - fields: Contains the form view ids of every form that has an error.
     *
     * @param array $errors [optional]
     */
    public function getFormErrorFieldsAndMessages(FormInterface $form, array $errors = []): array
    {
        foreach ($form->getErrors() as $error) {
            if (!isset($errors['messages']) || !in_array($error->getMessage(), $errors['messages'])) {
                $errors['messages'][] = $error->getMessage();
            }
            if (!isset($errors['fields']) || !in_array($form->createView()->vars['id'], $errors['fields'])) {
                $errors['fields'][] = $form->createView()->vars['id'];
            }
        }
        foreach ($form->all() as $childForm) {
            $errors = $this->getFormErrorFieldsAndMessages($childForm, $errors);
        }

        return $errors;
    }
}
