<?php

namespace App\Service\AdminUser;

use App\Entity\AdminUser;
use App\Service\Mailer\MailerBaseService;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class AdminUserMailerService extends MailerBaseService
{
    private UrlGeneratorInterface $urlGenerator;
    private string $appTitle;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $urlGenerator, string $appTitle)
    {
        parent::__construct($mailer);
        $this->urlGenerator = $urlGenerator;
        $this->appTitle = $appTitle;
    }

    public function sendAccountCreationEmail(AdminUser $adminUser): void
    {
        $choosePasswordUrl = $this->urlGenerator->generate('admin_activate_account', ['confirmationToken' => $adminUser->getConfirmationToken()], UrlGeneratorInterface::ABSOLUTE_URL);
        $email = $this->createTemplatedEmail(
            'admin/admin_user/emails/created.html.twig',
            'admin/admin_user/emails/created.txt.twig',
            ['create_password_url' => $choosePasswordUrl, 'user_full_name' => $adminUser->getFullName()],
        )
            ->subject(sprintf('%s - Création de votre compte', $this->appTitle))
            ->to(new Address($adminUser->getEmail(), $adminUser->getFullName()))
        ;

        $this->send($email);
    }

    public function sendActivationConfirmationEmail(AdminUser $adminUser): void
    {
        $email = $this->createTemplatedEmail(
            'admin/admin_user/emails/activated.html.twig',
            'admin/admin_user/emails/activated.txt.twig',
            [
                'user_full_name' => $adminUser->getFullName(),
                'login_url' => $this->urlGenerator->generate('admin_login', [], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        )
            ->subject(sprintf('%s - Votre compte est activé', $this->appTitle))
            ->to(new Address($adminUser->getEmail(), $adminUser->getFullName()))
        ;

        $this->send($email);
    }
}
