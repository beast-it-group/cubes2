<?php

namespace App\Service\AdminUser;

use App\Dto\AdminUserDto;
use App\Dto\DtoInterface;
use App\Entity\AdminUser;
use App\Repository\AdminUserRepository;
use App\Repository\UserRepository;
use App\Service\Dto\DtoFactoryInterface;
use App\Util\TokenGenerator;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class AdminUserFactory implements DtoFactoryInterface
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher,
        private ValidatorInterface          $validator,
        private AdminUserRepository         $adminUserRepository,
        private UserRepository              $userRepository
    )
    {
    }

    /**
     * @param AdminUserDto $adminUserDto
     * @param string $validationGroup
     * @return ConstraintViolationListInterface
     */
    public
    function validateDto(DtoInterface $adminUserDto, string $validationGroup = 'Default'): ConstraintViolationListInterface
    {
        $errors = $this->validator->validate($adminUserDto, null, $validationGroup);
        // We have to manually verify if a AdminUser is already existing in database since UniqueEntity constraint can only be applied on mapped Entity...
        // This issue might be fix in v6.1 TODO Update code by using UniqueEntity constraint when available
        $existingAdminUser = $this->adminUserRepository->findOneBy(['email' => $adminUserDto->getEmail()]);
        $existingUser = $this->userRepository->findOneBy(['email' => $adminUserDto->getEmail()]);
        if (
            ($existingAdminUser && !$adminUserDto->getId()) ||
            ($existingAdminUser && $adminUserDto->getId() !== $existingAdminUser->getId()) ||
            ($existingUser && !$adminUserDto->getId()) ||
            ($existingUser && $adminUserDto->getId() !== $existingUser->getId())
        ) {
            $errors->add(new ConstraintViolation(
                sprintf("L'email %s est déjà utilisé", $adminUserDto->getEmail()),
                "L'email {{ value }} est déjà utilisé",
                ['{{ value }}' => $adminUserDto->getEmail()],
                $adminUserDto,
                'email',
                $adminUserDto->getEmail()
            ));
        }

        return $errors;
    }

    /**
     * @param AdminUserDto $adminUserDto
     * @return AdminUser
     */
    public
    function createFromDto(DtoInterface $adminUserDto): AdminUser
    {
        $adminUser = (new AdminUser())
            ->setEmail($adminUserDto->getEmail())
            ->setFirstName($adminUserDto->getFirstName())
            ->setLastName($adminUserDto->getLastName())
            ->setRoles([$adminUserDto->getRole()])
            ->setActive($adminUserDto->isActive());

        if (!$adminUserDto->isActive()) {
            $adminUser->setConfirmationToken(TokenGenerator::generateToken(16));
        }

        if ($adminUserDto->getPlainPassword()) {
            $adminUser->setPassword($this->passwordHasher->hashPassword($adminUser, $adminUserDto->getPlainPassword()));
        } else {
            $adminUser->setPassword($this->passwordHasher->hashPassword($adminUser, strtoupper(md5(uniqid()))));
        }

        return $adminUser;
    }

    /**
     * @param AdminUserDto $adminUserDto
     */
    public
    function updateFromDto(DtoInterface $adminUserDto): AdminUser
    {
        $adminUser = $this->adminUserRepository->find($adminUserDto->getId());
        if (!$adminUser) {
            throw new UserNotFoundException(sprintf('Unable to find AdminUser object with id "%s"', $adminUserDto->getId()), 500);
        }
        $reflectionDto = new \ReflectionClass($adminUserDto);
        foreach ($reflectionDto->getProperties() as $property) {
            if (!in_array($property->getName(), ['plainPassword', 'role', 'active', 'id', 'avatar', 'avatarFile'])) {
                $setMethod = 'set' . ucfirst($property->name);
                $getMethod = 'get' . ucfirst($property->getName());
                if ($adminUser->$getMethod() !== $adminUserDto->$getMethod()) {
                    $adminUser->$setMethod($adminUserDto->$getMethod());
                }
            }
        }
        if ($adminUserDto->getAvatarFile()) {
            $adminUser->setAvatarFile($adminUserDto->getAvatarFile());
            $adminUser->setAvatar($adminUserDto->getAvatarFile()->getFilename());
        }
        $adminUser->setActive($adminUserDto->isActive());

        if ($adminUserDto->getRole() && $adminUser->getRoles()[0] !== $adminUserDto->getRole()) {
            $adminUser->setRoles([$adminUserDto->getRole()]);
        }

        if ($adminUserDto->getPlainPassword()) {
            $adminUser->setPassword($this->passwordHasher->hashPassword($adminUser, $adminUserDto->getPlainPassword()));
        }

        return $adminUser;
    }

    public
    function convertToDto(string $identifier, int|string $value): DtoInterface
    {
        $adminUser = $this->adminUserRepository->findOneBy([$identifier => $value]);
        if (!$adminUser) {
            throw new UserNotFoundException(sprintf('Unable to find AdminUser object from property "%s" with value "%s"', $identifier, $value), 500);
        }

        return (new AdminUserDto($adminUser->getId()))
            ->setActive($adminUser->isActive())
            ->setRole($adminUser->getRoles()[0])
            ->setEmail($adminUser->getEmail())
            ->setLastName($adminUser->getLastName())
            ->setFirstName($adminUser->getFirstName())
            ->setAvatar($adminUser->getAvatar());
    }
}
