<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public const USER_REFERENCE = 'user_reference';

    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $users = [];

        for ($i = 0; $i < 10; $i++) {
            $firstName = $faker->firstName();
            $lastName = $faker->lastName();
            $user = (new User())
                ->setFirstName($firstName)
                ->setLastName($lastName)
                ->setActive(true)
                ->setEmail(strtolower($firstName).'.'.strtolower($lastName).'@viacesi.fr')
                ->setPlainPassword('Test1234')
                ->setRoles([User::ROLE_DEFAULT])
                ;

            $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));

            $manager->persist($user);
            $users[] = $user;
        }

        $manager->flush();

        foreach ($users as $index => $user) {
            $this->addReference(sprintf('%s', self::USER_REFERENCE.'-'.$index), $user);
        }

    }

    public function getDependencies(): array
    {
        return [
            AdminUserFixtures::class,
        ];
    }
}