<?php

namespace App\DataFixtures;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use App\Entity\Group;
use App\Entity\Resource;
use App\Entity\User;
use App\Service\AdminUser\AdminUserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class TestFixtures extends Fixture implements FixtureGroupInterface
{
    public function __construct(private UserPasswordHasherInterface $passwordHasher,
                                private AdminUserFactory $adminUserFactory)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $user = (new User())
            ->setFirstName('John')
            ->setLastName('Doe')
            ->setActive(true)
            ->setEmail('johndoe@cubes.com')
            ->setPlainPassword('Test1234')
            ->setRoles([User::ROLE_DEFAULT]);
        $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));

        $manager->persist($user);

        $superAdminUserDto = (new AdminUserDto())
            ->setEmail('admin@beast.it')
            ->setPlainPassword('Test1234')
            ->setLastName('BeastIt')
            ->setFirstName('Admin')
            ->setActive(true)
            ->setRole(AdminUser::ROLE_SUPER_ADMIN)
        ;
        $superAdminUser = $this->adminUserFactory->createFromDto($superAdminUserDto);
        $manager->persist($superAdminUser);

        $resource = (new Resource())
            ->setName('ResourceTest_ReadingNote')
            ->setType(Resource::TYPE_READING_NOTE)
            ->setActive(true)
            ->setFile('test.png')
            ->setAuthor($user);
        $resource->setCreatedAt();
        $resource->setUpdatedAt();

        $manager->persist($resource);

        foreach (Group::TYPE_CHOICES as $type) {
            $group = (new Group())
                ->setName('Groupe '.$type)
                ->setType($type)
                ->addUser($user);
            $group->setCreatedAt();
            $group->setUpdatedAt();
            if (Group::TYPE_PUBLIC == $group->getType()) {
                $group->addResource($resource);
            }
            $manager->persist($group);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['test'];
    }
}
