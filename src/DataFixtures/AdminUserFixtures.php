<?php

namespace App\DataFixtures;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use App\Service\AdminUser\AdminUserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AdminUserFixtures extends Fixture
{
    private AdminUserFactory $adminUserFactory;

    public function __construct(AdminUserFactory $adminUserFactory)
    {
        $this->adminUserFactory = $adminUserFactory;
    }

    public function load(ObjectManager $manager): void
    {
        $superAdminUserDto = (new AdminUserDto())
            ->setEmail('admin@beast.it')
            ->setPlainPassword('Test1234')
            ->setLastName('BeastIt')
            ->setFirstName('Admin')
            ->setActive(true)
            ->setRole(AdminUser::ROLE_SUPER_ADMIN)
        ;
        $superAdminUser = $this->adminUserFactory->createFromDto($superAdminUserDto);
        $manager->persist($superAdminUser);

        $manager->flush();
    }
}
