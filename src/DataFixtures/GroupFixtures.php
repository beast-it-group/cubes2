<?php

namespace App\DataFixtures;

use App\Entity\Group;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class GroupFixtures extends Fixture implements DependentFixtureInterface
{
    public const GROUP_REFERENCE = 'group_reference';

    public function load(ObjectManager $manager): void
    {
        $groups = [];
        foreach (Group::TYPE_CHOICES as $type) {
            $group = (new Group())
                ->setName('Groupe ' . $type)
                ->setType($type);

            if ($group->getType() == Group::TYPE_PUBLIC) {
                for ($i = 0; $i < 10; $i++) {
                    $group->addUser($this->getReference(sprintf(UserFixtures::USER_REFERENCE.'-%s', $i)));
                }
            } else {
                for ($i = 0; $i < rand(0,9); $i++) {
                    $group->addUser($this->getReference(sprintf(UserFixtures::USER_REFERENCE.'-%s', rand(0, 9))));
                }
            }
            $group->setCreatedAt();
            $group->setUpdatedAt();
            $manager->persist($group);
            $groups[]= $group;

        }
        $manager->flush();

        foreach ($groups as $index => $group) {
            $this->addReference(sprintf('%s', self::GROUP_REFERENCE . '-' . $index), $group);
        }
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}