<?php

namespace App\DataFixtures;

use App\Entity\Resource;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResourceFixtures extends Fixture implements DependentFixtureInterface
{
    public const RESOURCE_REFERENCE = 'resource_reference';

    public function load(ObjectManager $manager): void
    {
        $names = [
            'Vidéo Marrante',
            'Vidéo pas marrante',
            'Livre',
            'Livre aussi',
            'Livre moyen',
            'Tutoriel vidéo',
            'Tutoriel',
            'Tutoriel plutôt correct'
        ];

        $resources = [];

        for ($i = 0; $i < 10; $i++) {
            $name = array_rand($names);

            $resource = (new Resource())
                ->setName($names[$name])
                ->setType(str_contains(strtolower($names[$name]), 'vidéo') ? Resource::TYPE_VIDEO : Resource::TYPE_CHOICES[array_rand(Resource::TYPE_CHOICES)])
                ->setActive(true)
            ;

            if ($resource->getType() == Resource::TYPE_VIDEO) {
                $resource->setVideoUrl('https://www.youtube.com/watch?v=Gl08Eove2us');
            } else {
                $resource->setFile('test.png');
            }
            $resource->setAuthor($this->getReference(sprintf(UserFixtures::USER_REFERENCE.'-%s', rand(0, 9))));
            $resource->setCreatedAt();
            $resource->setUpdatedAt();
            $group = $this->getReference(sprintf(GroupFixtures::GROUP_REFERENCE.'-%s', 0));
            $group->addResource($resource);
            $manager->persist($resource);
            $resources[] = $resource;

        }
        $manager->flush();

        foreach ($resources as $index => $resource) {
            $this->addReference(sprintf('%s', self::RESOURCE_REFERENCE.'-'.$index), $resource);
        }
    }


    public function getDependencies(): array
    {
        return [
            GroupFixtures::class,
        ];
    }
}