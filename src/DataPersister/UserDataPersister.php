<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\ResumableDataPersisterInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\User\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class UserDataPersister implements ContextAwareDataPersisterInterface, ResumableDataPersisterInterface
{
    private $_request;
    public function __construct(private EntityManagerInterface             $em,
                                private ContextAwareDataPersisterInterface $decorated,
                                private UserRepository                     $userRepository,
                                private RequestStack $request,
                                private UserService $userService
    )
    {
        $this->_request = $request->getCurrentRequest();
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function persist($data, array $context = []): void
    {
        if ($this->_request->getMethod() == 'POST') {
            $user = $this->userService->create($data);
        } else {
            $user = $this->userService->update($data);
        }

        $this->em->persist($user);
        $this->em->flush();
    }

    public function remove($data, array $context = []): void
    {
        $this->em->remove($data);
        $this->em->flush();
    }

    public function resumable(array $context = []): bool
    {
        return true;
    }
}