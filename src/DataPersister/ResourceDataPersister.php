<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\DataPersister\ResumableDataPersisterInterface;
use App\Entity\Resource;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ResourceDataPersister implements ContextAwareDataPersisterInterface, ResumableDataPersisterInterface
{
    private ?Request $_request;
    public function __construct(private EntityManagerInterface             $em,
                                private ContextAwareDataPersisterInterface $decorated,
                                private RequestStack $request,
    )
    {
        $this->_request = $request->getCurrentRequest();
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Resource;
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return mixed|object|void
     */
    public function persist($data, array $context = [])
    {
        $this->em->persist($data);
        $this->em->flush();

        return $data;
    }

    public function remove($data, array $context = []): void
    {
        $this->em->remove($data);
        $this->em->flush();
    }

    public function resumable(array $context = []): bool
    {
        return true;
    }
}
{

}