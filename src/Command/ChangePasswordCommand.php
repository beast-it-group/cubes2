<?php

namespace App\Command;

use App\Repository\AdminUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'badmin:admin-user:change-password',
    description: 'Change the password for an AdminUser',
)]
class ChangePasswordCommand extends Command
{

    private EntityManagerInterface $em;
    private UserPasswordHasherInterface $userPasswordHasher;
    private AdminUserRepository $adminUserRepository;

    public function __construct(EntityManagerInterface $em, AdminUserRepository $adminUserRepository, UserPasswordHasherInterface $userPasswordHasher, string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->adminUserRepository = $adminUserRepository;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::REQUIRED, 'AdminUser username')
            ->addArgument('newPassword', InputArgument::REQUIRED, 'AdminUser new password to set')
            ->setHelp(<<<'EOT'
The <info>oadmin:admin-user:change-password</info> command changes the password of an AdminUser:

  <info>php %command.full_name% username</info>

If not given, the interactive shell will ask you for a new password.

You can alternatively specify the password as a second argument:

  <info>php %command.full_name% username new-password</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $username = $input->getArgument('username');
        $newPassword = $input->getArgument('newPassword');

        $adminUser = $this->adminUserRepository->findOneBy(['email' => $username]);
        if (!$adminUser) {
            if ($output->isVerbose()) {
                $io->error(sprintf('AdminUser with username "%s" not found.', $username));
            }

            return Command::FAILURE;
        }

        $adminUser->setPassword($this->userPasswordHasher->hashPassword($adminUser, $newPassword));
        $this->em->flush();

        if ($output->isVerbose()) {
            $io->success(sprintf('Password for AdminUser "%s" has been changed.', $username));
        }

        return Command::SUCCESS;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $arguments = [];
        $io = new SymfonyStyle($input, $output);

        if (!$input->getArgument('username')) {
            $username = $io->ask('Choose a username (email) ', null, function (?string $username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }

                return $username;
            });
            $arguments['username'] = $username;
        }

        if (!$input->getArgument('newPassword')) {
            $newPassword = $io->askHidden('Enter the new password ', function (?string $password) {
                if (empty($password)) {
                    throw new \Exception('Password can not be empty');
                }

                return $password;
            });
            $arguments['newPassword'] = $newPassword;
        }

        foreach ($arguments as $name => $answer) {
            $input->setArgument($name, $answer);
        }
    }
}
