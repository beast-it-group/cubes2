<?php

namespace App\Command;

use App\Dto\AdminUserDto;
use App\Entity\AdminUser;
use App\Service\AdminUser\AdminUserFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'badmin:admin-user:create',
    description: 'Create a new AdminUser',
)]
class CreateAdminUserCommand extends Command
{

    private EntityManagerInterface $em;
    private AdminUserFactory $adminUserFactory;

    public function __construct(EntityManagerInterface $em, AdminUserFactory $adminUserFactory, string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->adminUserFactory = $adminUserFactory;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('username', InputArgument::REQUIRED, 'Username (must be an email address)')
            ->addArgument('password', InputArgument::REQUIRED, 'Password for the new AdminUser')
            ->addArgument('lastName', InputArgument::REQUIRED, "AdminUser's last name")
            ->addArgument('firstName', InputArgument::REQUIRED, "AdminUser's first name")
            ->addOption('super-admin', null, InputOption::VALUE_NONE, sprintf('Set the %s for AdminUser', AdminUser::ROLE_SUPER_ADMIN))
            ->setHelp(<<<'EOT'
The <info>oadmin:admin-user:create</info> command creates a new AdminUser:

  <info>php %command.full_name% username</info>

This interactive shell will ask you for a username (email), a password, a first name and a last name.

You can alternatively specify the username (email) and password as the first and second arguments:

  <info>php %command.full_name% username mypassword</info>

You can create a super admin via the super-admin flag:

  <info>php %command.full_name% username --super-admin</info>

EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $adminUserDto = (new AdminUserDto())
            ->setEmail($input->getArgument('username'))
            ->setPlainPassword($input->getArgument('password'))
            ->setLastName($input->getArgument('lastName'))
            ->setFirstName($input->getArgument('firstName'))
            ->setActive(true)
            ->setRole($input->getOption('super-admin') ? AdminUser::ROLE_SUPER_ADMIN : AdminUser::ROLE_ADMIN)
        ;

        $errors = $this->adminUserFactory->validateDto($adminUserDto, 'CommandCreate');
        if (0 === $errors->count()) {
            $adminUser = $this->adminUserFactory->createFromDto($adminUserDto);
            $this->em->persist($adminUser);
            $this->em->flush();
        } else {
            if ($output->isVerbose()) {
                $io->error(sprintf('%s', $errors));
            }

            return Command::FAILURE;
        }

        if ($output->isVerbose()) {
            $io->success(sprintf('AdminUser with email %s has been created !', $adminUser->getUserIdentifier()));
        }

        return Command::SUCCESS;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $requiredArguments = [];
        $io = new SymfonyStyle($input, $output);

        if (!$input->getArgument('username')) {
            $chosenUsername = $io->ask('Please choose a username (email) ', null, function (string $username) use ($io) {
                if (empty($username)) {
                    $io->error('Username can not be empty !');

                    return Command::FAILURE;
                }

                return $username;
            });
            $requiredArguments['username'] = $chosenUsername;
        }

        if (!$input->getArgument('password')) {
            $chosenPassword = $io->askHidden('Please choose a password ', function (string $password) use ($io) {
                if (empty($password)) {
                    $io->error('Password can not be empty !');

                    return Command::FAILURE;
                }

                return $password;
            });
            $requiredArguments['password'] = $chosenPassword;
        }

        $chosenLastName = $io->ask("User's last name ", null, function (string $lastName) use ($io) {
            if (empty($lastName)) {
                $io->error('Last name is required for a new AdminUser');

                return Command::FAILURE;
            }

            return $lastName;
        });
        $requiredArguments['lastName'] = $chosenLastName;

        $chosenLastName = $io->ask("User's first name ", null, function (string $lastName) use ($io) {
            if (empty($lastName)) {
                $io->error('First name is required for a new AdminUser');

                return Command::FAILURE;
            }

            return $lastName;
        });
        $requiredArguments['firstName'] = $chosenLastName;

        foreach ($requiredArguments as $argName => $value) {
            $input->setArgument($argName, $value);
        }
    }
}
