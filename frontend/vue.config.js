module.exports = {
  pwa: {
    name: 'Ressources Relationnelles',
    themeColor: '#00898E',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  }
}
