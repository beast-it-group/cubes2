import store from '@/store';

export default async (to, from, next) => {
  await store.dispatch('checkIfUserTokenExists');
  if (!store.getters.isLoggedIn) {
    return next({ name: 'LoginView' });
  }
  return next();
};
