import userService from '@/services/user.service';
import router from '@/router';

const userModule = {
  state: {
    user: null,
    response: null,
    isLoggingIn: false,
    isLoggingOut: false,
    isSigningUp: false,
  },
  getters: {
    isLoggedIn(state) {
      return state.user !== null;
    },
  },
  mutations: {
    loginRequest(state) {
      state.isLoggingIn = true;
      state.user = null;
      state.response = null;
    },
    loginSuccess(state, user) {
      state.isLoggingIn = false;
      state.user = user;
      state.response = null;
    },
    loginFailure(state, response) {
      state.isLoggingIn = false;
      state.user = null;
      state.response = response;
    },
    logoutRequest(state, user) {
      state.isLoggingOut = true;
      state.user = user;
      state.response = null;
    },
    logoutSuccess(state) {
      state.isLoggingOut = false;
      state.user = null;
      state.response = null;
    },
    logoutFailure(state, user, response) {
      state.isLoggingOut = false;
      state.user = user;
      state.response = response;
    },
    signupRequest(state) {
      state.isSigningUp = true;
      state.user = null;
      state.response = null;
    },
    signupSuccess(state, user) {
      state.isSigningUp = false;
      state.user = user;
      state.response = null;
    },
    signupFailure(state, response) {
      state.isSigningUp = false;
      state.user = null;
      state.response = response;
    },
  },
  actions: {
    async checkIfUserTokenExists({ commit }) {
      try {
        const response = await userService.checkIfUserTokenExists();
        return response
          ? commit('loginSuccess', response)
          : commit('loginFailure', response);
      } catch (error) {
        return console.error(error);
      }
    },

    async login({ commit }, { email, password }) {
      await commit('loginRequest');

      try {
        const response = await userService.login({ email, password });
        await commit('loginSuccess', response);
        return await router.push({ name: 'NewsFeedView' });
      } catch (error) {
        return await commit('loginFailure', error.response);
      }
    },

    async logout({ commit, state }) {
      await commit('logoutRequest', state.user);

      try {
        await userService.logout();
        return await commit('logoutSuccess');
      } catch (error) {
        return await commit('logoutFailure', state.user, error.response);
      }
    },

    async signup(
      { commit },
      { email, password, passwordConfirm, firstName, lastName }
    ) {
      await commit('signupRequest');

      try {
        const response = await userService.signup({
          email,
          plainPassword: password,
          passwordConfirm,
          firstName,
          lastName,
        });
        await commit('signupSuccess', response);
        return await router.push({ name: 'SignupSuccessView' });
      } catch (error) {
        return await commit('signupFailure', error.response);
      }
    },
  },
};

export default userModule;
