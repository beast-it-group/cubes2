import resourceService from '@/services/resource.service';
import router from '@/router';

const resourceModule = {
  state: {
    resource: null,
    isCreatingResource: false,
  },
  mutations: {
    createOneResourceRequest(state) {
      state.resource = null;
      state.isCreatingResource = true;
    },
    createOneResourceSuccess(state, resource) {
      state.resource = resource;
      state.isCreatingResource = false;
    },
    createOneResourceFailure(state) {
      state.resource = null;
      state.isCreatingResource = false;
    },
  },
  actions: {
    async createOneResource({ commit, rootState }, data) {
      await commit('fetchNewsFeedRequest');

      try {
        const response = await resourceService.createResource(
          data,
          rootState.user.user
        );
        await commit('fetchNewsFeedSuccess', response.data);
        await router.push({ name: 'NewsFeedView' });
      } catch (error) {
        await commit('fetchNewsFeedFailure');
      }
    },
  },
};

export default resourceModule;
