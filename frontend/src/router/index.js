import { createRouter, createWebHistory } from 'vue-router';

import isAlreadyLoggedGuard from '@/guards/is.already.logged.guard';
import checkAuthGuard from '@/guards/check.auth.guard';
import needsAuthGuard from '@/guards/needs.auth.guard';

const routes = [
  {
    path: '/',
    name: 'NewsFeedView',
    component: () => import('@/views/NewsFeedView.vue'),
    beforeEnter: checkAuthGuard,
  },
  {
    path: '/login',
    name: 'LoginView',
    component: () => import('@/views/Account/LoginView.vue'),
    beforeEnter: isAlreadyLoggedGuard,
  },
  {
    path: '/signup',
    name: 'SignupView',
    component: () => import('@/views/Account/SignupView.vue'),
    beforeEnter: isAlreadyLoggedGuard,
  },
  {
    path: '/signup/success',
    name: 'SignupSuccessView',
    component: () => import('@/views/Account/SignupSuccessView.vue'),
    beforeEnter: isAlreadyLoggedGuard,
  },
  {
    path: '/resources/create',
    name: 'ResourceCreateView',
    component: () => import('@/views/ResourceCreateView.vue'),
    beforeEnter: needsAuthGuard,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
