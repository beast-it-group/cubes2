import apiService from '@/services/api.service';

export default {
  async login({ email, password }) {
    const url = 'login';
    const response = await apiService.post(url, { email, password });
    localStorage.setItem('userToken', `Bearer ${response.data.token}`);
    return response;
  },

  async checkIfUserTokenExists() {
    return localStorage.getItem('userToken');
  },

  async logout() {
    return localStorage.removeItem('userToken');
  },

  async signup({ email, plainPassword, passwordConfirm, firstName, lastName }) {
    const url = 'users';
    return await apiService.post(url, {
      email,
      plainPassword,
      passwordConfirm,
      firstName,
      lastName,
    });
  },
};
