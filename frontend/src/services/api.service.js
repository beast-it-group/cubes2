import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 5000,
});

export default {
  async get(url) {
    return await axiosInstance.get(url, {
      headers: { Accept: 'application/json' },
    });
  },

  async post(url, data) {
    return await axiosInstance.post(url, data, {
      headers: { Accept: 'application/json' },
    });
  },

  async postMultipart(url, data, userToken) {
    return await axiosInstance.post(url, data, {
      headers: {
        Accept: 'application/json',
        Authorization: userToken,
        // FIXME: The following header generates a CORS error for some reason.
        'Content-Type': 'multipart/form-data',
      },
    });
  },
};
