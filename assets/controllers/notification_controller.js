import { Controller } from 'stimulus';
import Notification from "../js/modules/Notifications";

/**
 * Controller for notification management.
 * Add a delay on every Notifications types to avoid collision.
 * A delay is also applied for every notification of a same type.
 */
/* stimulusFetch: 'lazy' */
export default class extends Controller {

    connect() {
        let notifications = JSON.parse(this.element.dataset.notifications);
        if (Object.prototype.toString.call(notifications) === '[object Object]') {
            let totalNotificationsTypes = Object.entries(notifications).length;
            for (const [label, value] of Object.entries(notifications)) {
                setTimeout(() => {
                    value.forEach((message, index) => {
                        setTimeout(() => Notification.show(label, message), (index + 1) * 100);
                    });
                }, totalNotificationsTypes * 200)
                totalNotificationsTypes--;
            }
        }
    }
}