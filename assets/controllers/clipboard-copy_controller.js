import {Controller} from 'stimulus';
import Notification from "../js/modules/Notifications";

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    static values = {
        successMsg: String
    };

    onClick(event) {
        event.preventDefault();
        let input = document.createElement('input');
        input.type = 'text';
        input.value = this.element.innerText;
        document.body.appendChild(input);
        if (this.element.innerText === input.value) {
            input.select();
            document.execCommand('copy');
            Notification.show('success', this.successMsgValue||'Contenu copié');
        } else {
            Notification.show('error', 'Erreur lors de la copie du contenu');
        }
        input.remove();
    }
}