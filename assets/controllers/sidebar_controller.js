import { Controller } from 'stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    connect() {
        if (document.getElementsByClassName('sidebar-menu')[0].classList.contains('-translate-x-72')) {
            this.element.dataset.status = 'closed';
        }
    }

    toggle() {
        this.element.classList.toggle('on');
        if (this.element.dataset.status === 'closed') {
            this.element.dataset.status = 'open';
            document.getElementsByClassName('sidebar-menu')[0].classList.remove('-translate-x-72');
        } else {
            this.element.dataset.status = 'closed';
            document.getElementsByClassName('sidebar-menu')[0].classList.add('-translate-x-72');
        }
    }
}