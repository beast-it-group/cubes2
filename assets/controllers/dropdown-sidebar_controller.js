import {Controller} from 'stimulus';

export default class extends Controller {

    toggle() {
        let container = this.element.parentElement;
        this.element.querySelector('.dropdown-icon').classList.toggle('rotate-180');
        container.querySelector('.dropdown-sidebar').classList.toggle('invisible');
        container.querySelector('.dropdown-sidebar').classList.toggle('h-0');
        container.querySelector('.dropdown-sidebar').classList.toggle('p-2');
        container.querySelector('.dropdown-sidebar').classList.toggle('mt-2');
        container.querySelector('.dropdown-sidebar').classList.toggle('opacity-0');
    }
}