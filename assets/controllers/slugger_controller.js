import {Controller} from 'stimulus';
import axios from "axios";
import Notification from "../js/modules/Notifications";

export default class extends Controller {

    static values = {
        url: String,
    }
    static targets = ["elementToSlug", "sluggedElement"]
    connect() {
        this.timeout = null;
        this.timeToWait = 500;
        this.elementToSlugTarget.addEventListener('keyup', this.generateSlug.bind(this));
        document.querySelector('form').addEventListener('submit', () => {
            this.sluggedElementTarget.removeAttribute('disabled');
        });
    }

    generateSlug() {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            return this.ajaxSlug();
        }, this.timeToWait);
    }

    ajaxSlug() {
        let elementToSlug = this.elementToSlugTarget.value
        if (elementToSlug === undefined || this.sluggedElementTarget.value === undefined) {
            elementToSlug = '';
        }
        this.sluggedElementTarget.value = elementToSlug;
        this.sluggedElementTarget.setAttribute('disabled', true);

        axios.post(this.urlValue, {elementToSlug: elementToSlug})
            .then((response) => {
                this.sluggedElementTarget.value = response.data.sluggedElement;
            })
            .catch((error) => {
                Notification.show('error', "Une erreur système est survenue. Impossible de générer une url automatiquement.");
            }).then(() => {
            this.sluggedElementTarget.removeAttribute('disabled');
        })
        ;
    }
}