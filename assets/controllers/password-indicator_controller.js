import { Controller } from 'stimulus';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    connect() {
        this.element.insertAdjacentElement('afterend', this.createProgressBar());
        this.element.addEventListener('keyup', this.updateIndicator);
    }

    createProgressBar() {
        const progressBarContainer = document.createElement('div');
        progressBarContainer.classList.add('password-meter');
        const progressBar = document.createElement('div');
        progressBar.classList.add('determinate');
        progressBarContainer.appendChild(progressBar);

        return progressBarContainer;
    }

    updateIndicator(event) {
        let inputElement = event.currentTarget;
        let lowercaseRegex = /[a-z]/g;
        let uppercaseRegex = /[A-Z]/g;
        let digitRegex = /[0-9]/g;
        let specialCharRegex = /[!?@#$%^\\\/&*)(+=.,:;_-]/g;
        let value = inputElement.value.toString();
        let lengthConstraint = value.length;
        let progressBarWidth = 0;
        let progressBar = inputElement.parentNode.querySelector('.determinate');

        if (null !== value.match(lowercaseRegex)) {
            progressBarWidth += 20;
        }
        if (null !== value.match(uppercaseRegex)) {
            progressBarWidth += 20;
        }
        if (null !== value.match(digitRegex)) {
            progressBarWidth += 20;
        }
        if (null !== value.match(specialCharRegex)) {
            progressBarWidth += 20;
        }
        if (lengthConstraint >= 8) {
            progressBarWidth += 20;
        }

        progressBar.style.width = progressBarWidth + '%';
        progressBar.classList.toggle('valid', progressBarWidth === 100);
        progressBar.classList.toggle('invalid', progressBarWidth < 100);
    }
}