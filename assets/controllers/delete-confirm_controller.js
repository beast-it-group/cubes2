import { Controller } from 'stimulus';
import Swal from "sweetalert2";

/* stimulusFetch: 'lazy' */
export default class extends Controller {
    onSubmit(event) {
        event.preventDefault();
        Swal.fire({
            title: this.titleValue || 'Êtes vous sûr ?',
            text: "Cette opération ne peut pas être annulée",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#50B2AF',
            cancelButtonColor: '#DA003F',
            confirmButtonText: this.confirmTextValue || 'Oui, supprimer',
            cancelButtonText: 'Annuler',
            heightAuto: false,
            showLoaderOnConfirm: true,
            preConfirm: async () => {
                return this.element.submit();
            }
        }).then((result) => {
            if (undefined !== result.value && result.value.error) {
                Swal.fire({
                    title: 'Une erreur est survenue...',
                    text: 'Veuillez essayer à nouveau',
                    icon: 'error',
                    confirmButtonColor: '#50B2AF',
                    confirmButtonText: 'Ok',
                    heightAuto: false,
                });
            }
        })
    }
}