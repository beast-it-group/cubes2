export default class Notification {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Public functions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows a toast, handles the multiple arguments combination. To see more about the possible combination, see
     * (https://github.com/paper-development/vanilla-toasts).
     */
    static show() {
        /*********** <Parsing arguments> ***********/
        let args;
        try {
            args = Notification._parseArgs(arguments);
        } catch (exception) {
            throw exception;
        }

        let content = args["content"];
        let type = args["type"];
        let options = args["options"];
        /*********** </Parsing arguments> ***********/

        /*********** <Showing the toast> ***********/
        try {
            Notification._show(type, content, options)
        } catch (exception) {
            throw exception;
        }
        /*********** </Showing the toast> ***********/
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private functions
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Shows a toast.
     * @param type Notification type (success, error, warning, or info)
     * @param text Notification text msg
     * @param options Notification options
     * @private
     */
    static _show(type, text, options) {

        /*********** <DOM Elements creation> ***********/
        let toast = Notification._createToastElement(type, text, options)
        document.documentElement.append(toast);
        /*********** </DOM Elements creation> ***********/

        /*********** <Positioning> ***********/
        let position = options["position"].split("-");
        let vPosition = position[0];
        let hPosition = position[1];

        switch (vPosition) {
            case "top":
                toast.style.top = "0";
                break;
            case "middle":
                toast.style.top = "calc(50vh - (" + toast.offsetHeight + "px / 2) - " + options["margin"] + "px)";
                break;
            case "bottom":
                toast.style.bottom = "0";
                break;
            default:
                throw "Notification: error, unknown vertical position attribute " + vPosition + ".";
        }

        switch (hPosition) {
            case "left":
                toast.style.left = "0";
                break;
            case "centre":
                toast.style.left = "calc(50vw - (" + options["width"] + "px / 2) - " + options["margin"] + "px)";
                break;
            case "right":
                toast.style.right = "0";
                break;
            default:
                throw "Notification: error, unknown horizontal position attribute " + hPosition + ".";
        }
        toast.style.width = options["width"] + "px";
        toast.style.margin = options["margin"] + "px";
        /*********** </Positioning> ***********/

        /*********** <Appearance> ***********/
        toast.style.color = options["color"];
        toast.style.backgroundColor = options["backgroundcolor"];
        toast.style.opacity = options["opacity"];
        /*********** </Appearance> ***********/

        /*********** <Auto-remove> ***********/
        Notification._remove(toast, options["duration"]);

        // Mouse enter event, we must stop the auto-remove process
        toast.addEventListener("mouseenter", function () {

            //Clearing the timeout on the toast
            Notification._cancelRemove(toast);

            // Stopping the progress bar width animation by setting the width to the current width
            if (toast.progressbarType !== "hidden" && toast.progressbarType !== undefined)
                toast.progressbarElement.style.width = toast.progressbarElement.clientWidth + "px";
        });

        // Mouse leave event, we must restart the auto-remove process
        toast.addEventListener("mouseleave", function () {

            //Resetting width and adapting transition duration
            if (toast.progressbarType !== "hidden" && toast.progressbarType !== undefined) {
                window.requestAnimationFrame(function () {
                    toast.progressbarElement.style.transition = "width " + (options["unfocusduration"] / 1000) + "s linear";
                    toast.progressbarElement.style.width = "0";
                });
            }

            //Setting a timeout to remove the toast after the delay is over.
            Notification._remove(toast, options["unfocusduration"]);
        });
        /*********** </Auto-remove> ***********/

        /*********** <Progressbar animation> ***********/
        if (toast.progressbarType !== "hidden" && toast.progressbarType !== undefined) {
            window.requestAnimationFrame(function () {
                toast.progressbarElement.style.transition = "width " + (options["duration"] / 1000) + "s linear";
                setTimeout(() => {
                    toast.progressbarElement.style.width = "0";
                }, 10)
            });
        }
        /*********** </Progressbar animation> ***********/

        /*********** <Moving other toasts out of the way> ***********/
        for (let toast of Notification.toasts[vPosition + "-" + hPosition]) {
            if (vPosition === "top") {
                let currentTop = toast.style.top;
                toast.style.top = (Number(currentTop.substring(0, currentTop.length - 2)) + toast.offsetHeight + options["margin"]) + "px";
            } else if (vPosition === "middle") {
                let currentTop = toast.style.top;
                toast.style.top = currentTop.substring(0, currentTop.length - 1) + " + " + (toast.offsetHeight + options["margin"]) + "px)";
            } else if (vPosition === "bottom") {
                let currentBottom = toast.style.bottom;
                toast.style.bottom = (Number(currentBottom.substring(0, currentBottom.length - 2)) + toast.offsetHeight + options["margin"]) + "px";
            }
        }
        /*********** </Moving other toasts out of the way> ***********/

        /*********** <Finishing> ***********/
        //Showing the toast
        window.requestAnimationFrame(function () {
            toast.classList.remove("hidden");
        });

        //Adding the toast in the toast list
        Notification.toasts[vPosition + "-" + hPosition].push(toast);
        /*********** </Finishing> ***********/
    }

    /**
     * Creates a toast DOM element ready to be used.
     * What is done, in details: Elements hierarchy creation, classes addition, progressbar & close button.
     * What is **not** done: All options, except for the close button and the progressbar.
     * @param type Notification type
     * @param text Notification text
     * @param options Notification options
     * @returns {HTMLDivElement}
     * @private
     */
    static _createToastElement(type, text, options) {

        let toastContainer = document.createElement("div");
        let contentContainer = document.createElement("div")
        let messageContainer = document.createElement("p");
        let notificationIcon = document.createElement("i");

        switch (type) {
            case "success" :
                notificationIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>`;
                toastContainer.classList.add("bg-green-500");
                break;
            case "error" :
                notificationIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>`;
                toastContainer.classList.add("bg-red-500");
                break;
            case "warning":
                notificationIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>`;
                toastContainer.classList.add("bg-orange-500");
                break;
            default:
                notificationIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>`;
                toastContainer.classList.add("bg-blue-500");
                break;
        }

        if (options["showclose"]) {
            /** @TODO Implement this functionality properly */
            let closeButtonElement = document.createElement("i");
            closeButtonElement.classList.add("close-button");
            toastContainer.closeButtonElement = closeButtonElement;
        }

        if (options["progressbar"] === "top" || options["progressbar"] === "bottom") {
            let progressBarElement = document.createElement("div");
            progressBarElement.classList.add("progressbar");
            toastContainer.progressbarElement = progressBarElement;
            toastContainer.progressbarType = options["progressbar"];
        }

        toastContainer.classList.add("notification-toast", "hidden");
        contentContainer.classList.add("content");
        contentContainer.classList.add("flex");
        contentContainer.classList.add("items-center");
        messageContainer.classList.add("message");

        if (options["showclose"])
            toastContainer.append(toastContainer.closeButtonElement);

        const progressBarChoices = ['top', 'bottom'];

        /*if (progressBarChoices.includes(options["progressbar"]))
            toastContainer.append(toastContainer.progressbarElement);*/

        contentContainer.append(notificationIcon);
        messageContainer.innerHTML = text;
        contentContainer.append(messageContainer);

        toastContainer.append(contentContainer);

        if (progressBarChoices.includes(options["progressbar"]))
            toastContainer.append(toastContainer.progressbarElement);

        /*if (options["progressbar"] === "bottom") {
            toastContainer.append(toastContainer.progressbarElement);
        }*/

        return toastContainer;
    }

    /**
     * Parses the arguments for the show method.
     * @param args The argument array
     * @returns An object containing the content, title and options with the keys in the previously given order.
     * @private
     */
    static _parseArgs(args) {
        /*********** <Default values> ***********/
        let type = "";
        let content = "";
        let options = Notification.options;
        /*********** </Default values> ***********/


        /*********** <Parsing arguments> ***********/
        if (args.length === 1) {
            type = args[0];
        } else if (args.length === 2) {
            type = args[0];
            content = args[1];
        } else if (args.length === 3) {
            type = args[0];
            content = args[1];
            options = {...options, ...args[2]};
        } else {
            throw "Notification: error, incorrect argument count, expected 1, 2 or 3. " + args.length + " given.";
        }
        /*********** </Parsing arguments> ***********/

        return {
            "content": content,
            "type": type,
            "options": options
        };
    }

    /**
     * Removes a toast.
     * @param toast The toast to remove.
     * @param delay The delay (in ms) before the removal.
     * @private
     */
    static _remove(toast, delay) {
        toast.removeTimeout = setTimeout(function () {
            toast.classList.add("slide-out");

            setTimeout(function () {
                toast.remove();
            }, 700);
        }, delay);
    }

    /**
     * Cancels a toast removal.
     * @param toast The toast to cancel the removal on.
     * @private
     */
    static _cancelRemove(toast) {
        clearTimeout(toast.removeTimeout);
    }
}

// This property holds all the toasts
Notification.toasts = {
    "top-left": [], "top-centre": [], "top-right": [],
    "middle-left": [], "middle-centre": [], "middle-right": [],
    "bottom-left": [], "bottom-centre": [], "bottom-right": [],
};

// Default options
Notification.options = {
    "width": 350,
    "margin": 10,
    "color": "#FFFFFF",
    "duration": 5000,
    "unfocusduration": 1000,
    "position": "top-right",
    "showclose": false,
    "progressbar": "bottom",
    "opacity": "1",
    "type" : "info"
};