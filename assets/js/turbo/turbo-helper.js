import Swal from "sweetalert2";
import tinymce from "tinymce";

const TurboHelper = class {
    constructor() {
        this.cleanViewBeforeSnapshot();
        this.initializeFadeTransitions();
    }

    cleanViewBeforeSnapshot() {
        document.addEventListener('turbo:before-cache', () => {
            // Remove any open sweetalert modal before Turbo snapshot
            if (Swal.isVisible()) {
                Swal.getPopup().style.animationDuration = '0ms'
                Swal.close();
            }
            tinymce.remove();

            // Remove notifications that have already been displayed before Turbo snapshot
            Array.from(document.getElementsByClassName('stream-notifications')).forEach((element) => {
                element.removeAttribute('data-controller');
                element.removeAttribute('data-notifications');
            });
        });
    }

    initializeFadeTransitions() {
        document.addEventListener('turbo:visit', () => {
            document.body.classList.add('turbo-loading');
        });

        document.addEventListener('turbo:before-render', (event) => {
            if (this.isPreviewRendered()) {
                event.detail.newBody.classList.remove('turbo-loading');
                requestAnimationFrame(() => {
                    document.body.classList.add('turbo-loading');
                });
            } else {
                const isRestoration = event.detail.newBody.classList.contains('turbo-loading');
                if (isRestoration) {
                    event.detail.newBody.classList.remove('turbo-loading');

                    return;
                }
                event.detail.newBody.classList.add('turbo-loading');
            }
        });

        document.addEventListener('turbo:render', () => {
            if (!this.isPreviewRendered()) {
                requestAnimationFrame(() => {
                    document.body.classList.remove('turbo-loading');
                });
            }
        });
    }

    isPreviewRendered() {
        return document.documentElement.hasAttribute('data-turbo-preview');
    }
}

export default new TurboHelper();