## Description 

En tant que ..., je veux pouvoir ..., afin de ...

### Critères d'acceptation
 - [page1](../../wikis/page1)
 
**Scenario :** 

- Étant donné que 
- Lorsque 
- Alors 
 
## Tâches à réaliser
 - [ ] .

## DOR (Definition of Ready)
Cette user story est considérée comme ready si elle remplit les conditions suivantes :
- [ ] avoir un titre
- [ ] avoir été créée en tant qu’issue sur GitLab
- [ ] avoir été catégorisée grace à un label
- [ ] avoir été liée à un Epic
- [ ] avoir été définie
- [ ] être intelligible
- [ ] avoir des tests d'acceptation
- [ ] avoir des critères d'acceptation
- [ ] avoir été priorisée
- [ ] avoir été estimée dans le temps
- [ ] avoir été  acceptée par l'équipe et le Product Owner

## DOD (Definition of Done)
Cette user story est considérée comme terminée lorsque :
- [ ] le développement correspondant a été fait
- [ ] le code respecte les conventions de nommage & règles de développement
- [ ] le développement correspondant a été versionné
- [ ] le code est commenté où nécessaire (docstring)
- [ ] le code a été refactorisé
- [ ] Le développement respecte le design si concerné
- [ ] ce développement a été testé par rapport aux tests d'acceptation sur un environnement déployé
- [ ] Le CI est OK en respectant le protocole de test
- [ ] il est livrable
- [ ] La merge request doit être « ready for review »
- [ ] il a fait l'objet d'un code review par un pair
- [ ] le développement est mergé
- [ ] La documentation technique est mise à jour
- [ ] La merge request est fermée
- [ ] toutes les tâches associées sont terminées (avant de livrer, il faut s'assurer auprès des différents intervenants que rien n'a été laissé de côté, même si la responsabilité individuelle ne porte que sur son travail individuel) 


/label ~UserStory ~Backlog 
