const colors = require('tailwindcss/colors');

module.exports = {
    darkMode: 'class',
    mode: 'jit',
    purge: {
        content: [
            './templates/*.html.twig',
            './templates/*/.html.twig',
            './templates/*//.html.twig',
            './templates/*///.html.twig',
        ],
        safelist: ['bg-green-500', 'bg-red-500', 'bg-orange-500', 'bg-blue-500', 'bg-gray-400', 'rotate-180'],
    },
    theme: {
        colors: {
            blue: colors.blue,
            cyan: colors.cyan,
            gray: {
                '50': '#f9fafb',
                '100': '#f4f5f7',
                '200': '#e5e7eb',
                '300': '#d5d6d7',
                '400': '#9e9e9e',
                '500': '#707275',
                '600': '#4c4f52',
                '700': '#24262d',
                '800': '#1a1c23',
                '900': '#121317',
            },
            orange: colors.yellow,
            green: colors.green,
            red: colors.red,
            white: colors.white,
            indigo: colors.indigo,
            purple: colors.purple,
            pink: colors.pink,
            yellow: colors.yellow,

            appBgLight: {
                'white': '#FFFFFF',
                'gray': '#F9FAFA',
            },
            appBgDark: {
                'light': '#252328',
                'dark':'#1A1819',
            },
            appElement: {
                'light': '#88D4CB',
                'default': '#00989E',
                'lm-active': '#194D47',
                'lm-hover': '#88D4CB',
                'dm-active': '#88D4CB',
                'dm-hover': '#27625B',
            },
            transparent: 'transparent',
        },
        // boxShadow: {
        //     darkMode: 'inset 1px 1px 4px 2px rgba(0,0,0,0.64)\n',
        //     innerDarkMode: 'inset 0px 4px 6px 2px rgba(0,0,0,0.64)\n',
        //     LightMode: '1px 1px 4px 2px rgba(229,229,229,0.64)\n',
        //     innerLightMode: 'inset 0px 4px 6px 2px rgba(229,229,229,0.64)\n',
        // },
        // borderRadius: {
        //     'none': '0',
        //     'sm': '3px',
        //     DEFAULT: '5px',
        //     'large': '0.5rem',
        //     'full': '100%',
        //     'pill': '9999px',
        // },
        container: {
            center: true,
        },
        fontFamily: {
            body: ['"Roboto"', '"Work sans"', '"Montserrat"'],
        },
    },
    variants: {},
    plugins: [
        require('@tailwindcss/forms'),
    ],
}